package greenbullracing.driver;

import greenbullracing.Driver;
import greenbullracing.Message;
import greenbullracing.message.CarPositionsMessage;
import greenbullracing.message.GameInitMessage;
import greenbullracing.message.GameStartMessage;
import greenbullracing.message.PingMessage;
import greenbullracing.message.SwitchLaneMessage;
import greenbullracing.message.ThrottleMessage;
import greenbullracing.message.YourCarMessage;
import greenbullracing.message.data.SwitchDirection;
import greenbullracing.message.data.TrackLane;
import greenbullracing.message.data.TrackPiece;
import greenbullracing.route.Edge;
import greenbullracing.route.Graph;
import greenbullracing.route.Vertex;
import greenbullracing.track.Cars;
import greenbullracing.track.StatList;
import greenbullracing.track.Track;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import math.geom2d.spline.CubicBezierCurve2D;
import math.geom2d.spline.QuadBezierCurve2D;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class FrobDriver extends Driver {
	private static final Logger log=LoggerFactory.getLogger(FrobDriver.class);
	private static final Logger csvlog=LoggerFactory.getLogger("csv");

	private Cars cars;
	private boolean doPrintStats = true;
	private boolean second2 = true;

	@Override
	public void receive(Message message) throws IOException {
		if (message instanceof YourCarMessage) {
			this.myColor = ((YourCarMessage)message).getCarId().getColor();
		}

		if (message instanceof GameInitMessage) {
			createGraph(message);
//			track = new Track(message);
			track = new Track(graph.getEdges(), ((GameInitMessage)message).getGameInitData().getRace().getTrack().getPieces().size(), ((GameInitMessage)message).getGameInitData().getRace().getTrack().getLanes().size());
			cars = new Cars(track);
		}
                
                if (message instanceof GameStartMessage) {
                    upHandler.send(new ThrottleMessage(1.0d));
                }
		
		if(message instanceof CarPositionsMessage) {

			CarPositionsMessage cpm=(CarPositionsMessage)message;
			cars.update(cpm);
			
			if (cpm.getCarPositions().get(0).getPiecePosition().getLap() == 1 && doPrintStats) {
				StatList foo = cars.getCar(myColor).getStatLists();
				for (int i = 0; foo.getInPiecePosition().get(i) >= 0.0; i++) {
					log.trace("SL: {}, EL: {}: Speed: {}, angle: {}, accel: {}, pieceIndex: {}, inPiecePos: {}", foo.getStartLaneIndex().get(i), foo.getEndLaneIndex().get(i), foo.getSpeed().get(i), foo.getAngle().get(i), foo.getAcceleration().get(i), foo.getPieceIndex().get(i), foo.getInPiecePosition().get(i));
				}
				doPrintStats = false;
			}
                        
                        if(cpm.getGameTick() == 1) {
                            upHandler.send(new ThrottleMessage(1.0d));
                        }
                        
                        if(cpm.getGameTick() == 2) {
                            cars.getCar(myColor).setAccelerationSlope();
                        }

                        if(cpm.getGameTick() == 25) {
                            upHandler.send(new ThrottleMessage(0.0));
                            cars.getCar(myColor).setInitialSpeedForDecelerationEstimate();
                            log.trace("lastspeed: {}, lastacceleration: {}", cars.getCar(myColor).getLastSpeed(), cars.getCar(myColor).getLastAcceleration());
                        }
                        
			if(cpm.getGameTick() == 30) {
                            upHandler.send(new ThrottleMessage(1.0d));

                            log.trace("lastspeed: {}, lastacceleration: {}", cars.getCar(myColor).getLastSpeed(), cars.getCar(myColor).getLastAcceleration());
                            cars.getCar(myColor).estimateDecelerationSlope(5);
                            double carLastSpeed = cars.getCar(myColor).getLastSpeed();
                            log.trace("decSlope: {}, decAtFull: {}, calculated at {}: {}, maxAccel: {}", cars.getCar(myColor).getDecSlope(), cars.getCar(myColor).getDecAtFull(), carLastSpeed, cars.getCar(myColor).getDecAtSpeed(carLastSpeed), cars.getCar(myColor).getMaxAcceleration());

                        }
                        
			else if(cpm.getCarPositions().get(0).getPiecePosition().getPieceIndex()==1)
			{ upHandler.send(new ThrottleMessage(0.1d)); }
			else if(cpm.getCarPositions().get(0).getPiecePosition().getPieceIndex()==2 && second2) {
				upHandler.send(new ThrottleMessage(0.65d));
				second2 = false; }
			else if (cpm.getCarPositions().get(0).getPiecePosition().getPieceIndex()==2) {
				upHandler.send(new SwitchLaneMessage(SwitchDirection.RIGHT));
				second2 = true; }
			else if(cpm.getCarPositions().get(0).getPiecePosition().getPieceIndex()==7)
			{ upHandler.send(new SwitchLaneMessage(SwitchDirection.LEFT)); }
//			else if(cpm.getCarPositions().get(0).getPiecePosition().getPieceIndex()==12)
//				upHandler.send(new SwitchLaneMessage(SwitchDirection.LEFT));
//			else if(cpm.getCarPositions().get(0).getPiecePosition().getPieceIndex()==12)
//				upHandler.send(new SwitchLaneMessage(SwitchDirection.LEFT));
			else if(cpm.getCarPositions().get(0).getPiecePosition().getPieceIndex()==17)
				upHandler.send(new SwitchLaneMessage(SwitchDirection.RIGHT));
			else if(cpm.getCarPositions().get(0).getPiecePosition().getPieceIndex()==28)
				upHandler.send(new SwitchLaneMessage(SwitchDirection.LEFT));
//			else if(cpm.getCarPositions().get(0).getPiecePosition().getPieceIndex()==34)
//				upHandler.send(new SwitchLaneMessage(SwitchDirection.RIGHT));
			else
				upHandler.send(new PingMessage());
		}
	}

	private void createGraph(Message message) {
		GameInitMessage gim = (GameInitMessage)message;
		List<TrackPiece> pieceList = gim.getGameInitData().getRace().getTrack().getPieces();
		List<TrackLane> lanesList = new ArrayList<>(gim.getGameInitData().getRace().getTrack().getLanes());

		int lanecount = lanesList.size();
		int piececount = pieceList.size();
		List<Double> lanedistances = new ArrayList<>(lanecount);
		log.trace(" lanecount: {}", lanecount);
		log.trace("piececount: {}", piececount);

		List<Edge> edges = new ArrayList<>();
		List<Vertex> vertexen = new ArrayList<>();
		for (int i = 0; i < ((lanecount * piececount)); i++) {
			vertexen.add(i, new Vertex(i));
		}
		log.trace("vertexen: {}", vertexen.size());

		Collections.sort(lanesList, new Comparator<TrackLane>() {
			@Override
			public int compare(TrackLane o1, TrackLane o2) {
				if(o1.getDistanceFromCenter()<o2.getDistanceFromCenter())
					return -1;
				else if(o1.getDistanceFromCenter()>o2.getDistanceFromCenter())
					return 1;
				return 0;
			}
		});

		for (TrackLane lane : lanesList) {
//			lanedistances.add(lane.getIndex(), (-1.0 * lane.getDistanceFromCenter()));
			lanedistances.add(lane.getIndex(), (lane.getDistanceFromCenter()));
//			Collections.sort(lanedistances);
		}

		int processedPieces = 0;
		double angle = 0.0;
		double switchlength;
		for (TrackPiece tp : pieceList) {
			angle = tp.getAngle();
			boolean hasswitch = tp.isSwitchPresent();
			log.trace("angle: {}, hasswitch: {}", angle, hasswitch);
			if (angle == 0.0) {
				for (int i = 0; i < lanecount; i++) {
					edges.add(new Edge(vertexen.get(  ((processedPieces) * lanecount) + i), vertexen.get( (((processedPieces + 1) * lanecount) + i) % (lanecount * piececount)), tp.getLength(), processedPieces, i, i ));
					log.trace("Added edge: {}", edges.get(edges.size() - 1));
				}
				if (hasswitch) {
					int i = 0;
//					double switchlength = tp.getLength() * 1.1;  // 10% length penalty for lane switch
					do {
						switchlength = Math.sqrt((Math.pow(tp.getLength(), 2) + Math.pow(Math.abs(lanedistances.get(i) - lanedistances.get(i + 1)), 2)));
double laneDiff=Math.abs(lanedistances.get(i) - lanedistances.get(i + 1));
// calculate from center line to laneDiff instead of lane to lane.
CubicBezierCurve2D curve=new CubicBezierCurve2D(0,0, tp.getLength()/2,0, tp.getLength()/2,laneDiff, tp.getLength(),laneDiff);
log.debug("*** {}: straight {} Polyline bonanza: {} {} {} {} {}",processedPieces,tp.getLength(),curve.asPolyline(1).length(),curve.asPolyline(10).length(),curve.asPolyline(100).length(),curve.asPolyline(1000).length(),curve.asPolyline(10000).length());
						edges.add(new Edge(vertexen.get( ((processedPieces) * lanecount) + i), vertexen.get( (((processedPieces + 1) * lanecount) + i + 1) % (lanecount * piececount)), switchlength, processedPieces, i, i + 1 ));
						log.trace("Added edge: {}", edges.get(edges.size() - 1));
						i++;
					} while (i < lanecount - 1);
					while (i > 0) {
						switchlength = Math.sqrt((Math.pow(tp.getLength(), 2) + Math.pow(Math.abs(lanedistances.get(i) - lanedistances.get(i - 1)), 2)));
						edges.add(new Edge(vertexen.get( ((processedPieces) * lanecount) + i), vertexen.get( (((processedPieces + 1) * lanecount) + i - 1) % (lanecount * piececount)), switchlength, processedPieces, i, i - 1));
						log.trace("Added edge: {}", edges.get(edges.size() - 1));
						i--;
					}
				}
				processedPieces++;
				log.trace("Done {} pieces.", processedPieces);
				continue;
			}
			
			double radius = tp.getRadius();
			if (angle > 0) { Collections.reverse(lanedistances); };
			for (int i = 0; i < lanecount; i++) {
				edges.add(new Edge(vertexen.get( ((processedPieces) * lanecount) + i), vertexen.get( (((processedPieces + 1) * lanecount) + i) % (lanecount * piececount)), (Math.signum(angle) * (angle / 360) * ((lanedistances.get(i) + radius) * 2) * Math.PI), processedPieces, i, i ));
				log.trace("Added edge: {}", edges.get(edges.size() - 1));
			}
			
			double rangle = angle * Math.PI / 180;
			if (hasswitch) {
				int i = 0;
					do {
						double y = Math.sin((Math.signum(rangle) * rangle)) * (radius + lanedistances.get(i));
						double x = ((radius + lanedistances.get(i + 1))) - ((Math.cos(Math.signum(rangle) * rangle) * (radius + lanedistances.get(i))));
						switchlength = Math.sqrt((Math.pow(x, 2) + Math.pow(y, 2)));
						log.trace ("x {}, y {}, len {}, angle {}, pieceIndex {}, pieceradius {}, laneradius {}", x, y, switchlength, angle, processedPieces, radius, (radius + lanedistances.get(i)));

TrackLane leftLane=lanesList.get(i);
TrackLane rightLane=lanesList.get(i+1);
double turnSign=-Math.signum(tp.getAngle());
double leftRadius=tp.getRadius()+turnSign*leftLane.getDistanceFromCenter();
double rightRadius=tp.getRadius()+turnSign*rightLane.getDistanceFromCenter();
double localAngleToCenter=turnSign*90;
double laneMiddleRadius=tp.getRadius()+turnSign*(leftLane.getDistanceFromCenter()+rightLane.getDistanceFromCenter())/2;
double middleX=leftRadius*Math.cos(Math.toRadians(localAngleToCenter))+laneMiddleRadius*Math.cos(Math.toRadians(localAngleToCenter+180-tp.getAngle()/2));
double middleY=-leftLane.getDistanceFromCenter()+leftRadius*Math.sin(Math.toRadians(localAngleToCenter))+laneMiddleRadius*Math.sin(Math.toRadians(localAngleToCenter+180-tp.getAngle()/2));
double endX=leftRadius*Math.cos(Math.toRadians(localAngleToCenter))+rightRadius*Math.cos(Math.toRadians(localAngleToCenter+180-tp.getAngle()));
double endY=-leftLane.getDistanceFromCenter()+leftRadius*Math.sin(Math.toRadians(localAngleToCenter))+rightRadius*Math.sin(Math.toRadians(localAngleToCenter+180-tp.getAngle()));
QuadBezierCurve2D curve=new QuadBezierCurve2D(0,-leftLane.getDistanceFromCenter(), middleX,middleY, endX,endY);
log.debug("*** {}: curve {}/{}° Polyline bonanza: {} {} {} {} {}",processedPieces,tp.getRadius(),tp.getAngle(),curve.asPolyline(1).length(),curve.asPolyline(10).length(),curve.asPolyline(100).length(),curve.asPolyline(1000).length(),curve.asPolyline(10000).length());
//						edges.add(new Edge(vertexen.get( ((processedPieces) * lanecount) + i), vertexen.get( (((processedPieces + 1) * lanecount) + i + 1 ) % (lanecount * piececount)), switchlength, processedPieces, i, i + 1));
						edges.add(new Edge(vertexen.get( ((processedPieces) * lanecount) + i), vertexen.get( (((processedPieces + 1) * lanecount) + i + 1 ) % (lanecount * piececount)), curve.asPolyline(100).length(), processedPieces, i, i + 1));
						log.trace("Added edge: {}", edges.get(edges.size() - 1));
						i++;
					} while (i < lanecount - 1);
					while (i > 0) {
						double y = Math.sin((Math.signum(rangle) * rangle)) * (radius + lanedistances.get(i));
						double x = ((radius + lanedistances.get(i - 1))) - ((Math.cos(Math.signum(rangle) * rangle) * (radius + lanedistances.get(i))));
 						switchlength = Math.sqrt((Math.pow(x, 2) + Math.pow(y, 2)));
						log.trace ("x {}, y {}, len {}, angle {}, pieceIndex {}, pieceradius {}, laneradius {}", x, y, switchlength, angle, processedPieces, radius, (radius + lanedistances.get(i)));
TrackLane leftLane=lanesList.get(i-1);
TrackLane rightLane=lanesList.get(i);
double turnSign=-Math.signum(tp.getAngle());
double leftRadius=tp.getRadius()+turnSign*leftLane.getDistanceFromCenter();
double rightRadius=tp.getRadius()+turnSign*rightLane.getDistanceFromCenter();
double localAngleToCenter=turnSign*90;
double laneMiddleRadius=tp.getRadius()+turnSign*(leftLane.getDistanceFromCenter()+rightLane.getDistanceFromCenter())/2;
double middleX=leftRadius*Math.cos(Math.toRadians(localAngleToCenter))+laneMiddleRadius*Math.cos(Math.toRadians(localAngleToCenter+180-tp.getAngle()/2));
double middleY=-leftLane.getDistanceFromCenter()+leftRadius*Math.sin(Math.toRadians(localAngleToCenter))+laneMiddleRadius*Math.sin(Math.toRadians(localAngleToCenter+180-tp.getAngle()/2));
double endX=leftRadius*Math.cos(Math.toRadians(localAngleToCenter))+rightRadius*Math.cos(Math.toRadians(localAngleToCenter+180-tp.getAngle()));
double endY=-leftLane.getDistanceFromCenter()+leftRadius*Math.sin(Math.toRadians(localAngleToCenter))+rightRadius*Math.sin(Math.toRadians(localAngleToCenter+180-tp.getAngle()));
QuadBezierCurve2D curve=new QuadBezierCurve2D(0,-leftLane.getDistanceFromCenter(), middleX,middleY, endX,endY);
log.debug("*** {}: curve {}/{}° Polyline bonanza: {} {} {} {} {}",processedPieces,tp.getRadius(),tp.getAngle(),curve.asPolyline(1).length(),curve.asPolyline(10).length(),curve.asPolyline(100).length(),curve.asPolyline(1000).length(),curve.asPolyline(10000).length());
						edges.add(new Edge(vertexen.get( ((processedPieces) * lanecount) + i), vertexen.get( (((processedPieces + 1) * lanecount) + i -  1 ) % (lanecount * piececount)), curve.asPolyline(100).length(), processedPieces, i, i - 1 ));
//						edges.add(new Edge(vertexen.get( ((processedPieces) * lanecount) + i), vertexen.get( (((processedPieces + 1) * lanecount) + i -  1 ) % (lanecount * piececount)), switchlength, processedPieces, i, i - 1 ));
						log.trace("Added edge: {}", edges.get(edges.size() - 1));
						i--;
					}
			}
			if (angle > 0) { Collections.reverse(lanedistances); };
			processedPieces++;
			log.trace("Done {} pieces.", processedPieces);
		}
		log.debug("Done {} pieces.", processedPieces);

/* Following would epsilon transition. It's removed as unnecessary
 * for the time being.
 */
//		log.debug("Add edge from track end to beginning (=create cycle).");
//		for (int i = 0; i < lanecount; i++) {
//			edges.add(new Edge(vertexen.get(vertexen.size() - 1 - i), vertexen.get((lanecount) - 1 - i), 0.0d));
//			log.debug("Cycle edge: {}", edges.get(edges.size() - 1));
//		}
		this.graph = new Graph(vertexen, edges, gim.getGameInitData().getRace().getTrack().getLanes().size());
		log.debug("Graph has {} vertexen, {} edges.", vertexen.size(), edges.size());
		
		for (Edge e : edges) {
			log.trace("Edge: {}", e.toString());
		}
		
//		for (int i = 0; i < piececount; i++) {
//			for (int j = 0; j < lanecount; j++) {
//			log.trace("pieceIndex: {}, lane: {}, length: {}", i, j, track.getLaneLength(i, j));
//			}
//		}
	}
}
/*
0  {"length":100.0}
1  {"length":100.0}
2  {"length":100.0}
3  {"length":100.0,"switch":true}
4  {"radius":100,"angle":45.0}
5  {"radius":100,"angle":45.0}
6  {"radius":100,"angle":45.0}
7  {"radius":100,"angle":45.0}
8  {"radius":200,"angle":22.5,"switch":true}
9  {"length":100.0}
10 {"length":100.0}
11 {"radius":200,"angle":-22.5}
12 {"length":100.0}
13 {"length":100.0,"switch":true}
14 {"radius":100,"angle":-45.0}
15 {"radius":100,"angle":-45.0}
16 {"radius":100,"angle":-45.0}
17 {"radius":100,"angle":-45.0}
18 {"length":100.0,"switch":true}
19 {"radius":100,"angle":45.0}
20 {"radius":100,"angle":45.0}
21 {"radius":100,"angle":45.0}
22 {"radius":100,"angle":45.0}
23 {"radius":200,"angle":22.5}
24 {"radius":200,"angle":-22.5}
25 {"length":100.0,"switch":true}
26 {"radius":100,"angle":45.0}
27 {"radius":100,"angle":45.0}
28 {"length":62.0}
29 {"radius":100,"angle":-45.0,"switch":true}
30 {"radius":100,"angle":-45.0}
31 {"radius":100,"angle":45.0}
32 {"radius":100,"angle":45.0}
33 {"radius":100,"angle":45.0}
34 {"radius":100,"angle":45.0}
35 {"length":100.0,"switch":true}
36 {"length":100.0}
37 {"length":100.0}
38 {"length":100.0}
39 {"length":90.0}
*/
