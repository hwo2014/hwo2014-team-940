package greenbullracing.driver;

import greenbullracing.Message;
import greenbullracing.OnDriver;
import greenbullracing.message.GameStartMessage;
import greenbullracing.message.ThrottleMessage;

public class SteadyDriver extends OnDriver {

	protected boolean sendGameTick=true;
	protected double throttle=0.1D;

	public SteadyDriver() { }

	public SteadyDriver(double throttle) {
		this.throttle=throttle;
	}

	protected Message onGameStart(GameStartMessage message) {
		super.onGameStart(message);
		if(sendGameTick)
			return new ThrottleMessage(throttle,serverGameTick);
		else
			return new ThrottleMessage(throttle);
	}

}
