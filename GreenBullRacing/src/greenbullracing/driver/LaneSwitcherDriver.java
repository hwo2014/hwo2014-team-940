package greenbullracing.driver;

import greenbullracing.Message;
import greenbullracing.message.CarPositionsMessage;
import greenbullracing.message.GameInitMessage;
import greenbullracing.message.SwitchLaneMessage;
import greenbullracing.message.data.CarPosition;
import greenbullracing.message.data.SwitchDirection;
import greenbullracing.message.data.Track;
import greenbullracing.message.data.TrackLane;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Random;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class LaneSwitcherDriver extends SteadyDriver {
	private static final Logger log=LoggerFactory.getLogger(LaneSwitcherDriver.class);

	protected boolean sendGameTick=false;
	protected Track track;
	protected int waitingForSwitchPiece=-1;
	protected int currentLane=-1;
	protected List<TrackLane> lanes;
	protected Random rnd=new Random(System.currentTimeMillis());

	public LaneSwitcherDriver() {
		throttle=0.45D;
	}

	@Override
	protected Message onCarPositions(CarPositionsMessage message) {
		Message rm=super.onCarPositions(message);
		CarPosition carPosition=getCarPosition(message.getCarPositions(),myCarId);
		if(currentLane==-1)
			currentLane=getLaneArrayIndexByServerIndex(carPosition.getPiecePosition().getLane().getStartLaneIndex());

		int positionLane=getLaneArrayIndexByServerIndex(carPosition.getPiecePosition().getLane().getStartLaneIndex());
		if(positionLane!=currentLane) {
			// we have reached next lane
			currentLane=positionLane;
		}

		if(message.getGameTick()<0)
			return rm;
		int currentPieceIndex=carPosition.getPiecePosition().getPieceIndex();
		if(waitingForSwitchPiece==currentPieceIndex || waitingForSwitchPiece==-1) {
			int nextPieceIndex=currentPieceIndex+1;
			while(true) {
				if(nextPieceIndex>=track.getPieces().size())
					nextPieceIndex=0;
				if(nextPieceIndex==currentPieceIndex) {
					// no switches in the track?
					waitingForSwitchPiece=-1;
					break;
				}
				if(track.getPieces().get(nextPieceIndex).isSwitchPresent()) {
					waitingForSwitchPiece=nextPieceIndex;
					double prob=rnd.nextDouble();
					SwitchDirection sd=null;
					if(currentLane==0) {
						if(prob>0.50)
							sd=SwitchDirection.RIGHT;
					} else if(currentLane>=lanes.size()-1) {
						if(prob>0.50)
							sd=SwitchDirection.LEFT;
					} else
						if(prob<45)
							sd=SwitchDirection.LEFT;
						else if(prob<90)
							sd=SwitchDirection.RIGHT;
					if(sd!=null) {
						rm=new SwitchLaneMessage(sd);
						log.debug("switching lane from {} to {} at piece {}",currentLane, sd,nextPieceIndex);
					}
					break;
				}
				nextPieceIndex++;
			}
		}
		return rm;
	}

	protected int getLaneArrayIndexByServerIndex(int serverIndex) {
		for(int i=0; i<lanes.size(); i++)
			if(lanes.get(i).getIndex()==serverIndex)
				return i;
		return -1;
	}

	@Override
	protected Message onGameInit(GameInitMessage message) {
		track=message.getGameInitData().getRace().getTrack();
		lanes=new ArrayList<>(track.getLanes());
		Collections.sort(lanes, new Comparator<TrackLane>() {
			@Override
			public int compare(TrackLane o1, TrackLane o2) {
				if(o1.getDistanceFromCenter()<o2.getDistanceFromCenter())
					return -1;
				else if(o1.getDistanceFromCenter()>o2.getDistanceFromCenter())
					return 1;
				return 0;
			}
		});
		log.debug("lanes: {}",lanes);

		return super.onGameInit(message);
	}

}
