package greenbullracing.driver;

import greenbullracing.Driver;
import greenbullracing.Message;
import greenbullracing.message.CarPositionsMessage;
import greenbullracing.message.GameStartMessage;
import greenbullracing.message.ThrottleMessage;

import java.io.IOException;

public class NoobDriver extends Driver {

	private boolean leftLane=false;
	private boolean sendGameTick=false;

	@Override
	public void receive(Message message) throws IOException {
		if(message instanceof GameStartMessage || (message instanceof CarPositionsMessage && ((CarPositionsMessage)message).getGameTick()!=-1)) {
			// default value
			int gameTick=new ThrottleMessage().getGameTick();
			if(sendGameTick)
				if(message instanceof GameStartMessage)
					gameTick=((GameStartMessage)message).getGameTick();
				else
					gameTick=((CarPositionsMessage)message).getGameTick();
			if(leftLane)
				upHandler.send(new ThrottleMessage(0.655d,gameTick));
			else
				if(message instanceof CarPositionsMessage && ((CarPositionsMessage)message).getGameTick()==1)
					upHandler.send(new greenbullracing.message.SwitchLaneMessage(greenbullracing.message.data.SwitchDirection.RIGHT,gameTick));
				else
					upHandler.send(new greenbullracing.message.ThrottleMessage(0.651d,gameTick));
		}
	}

}
