package greenbullracing.driver;

import greenbullracing.Driver;
import greenbullracing.Message;
import greenbullracing.message.CarPositionsMessage;
import greenbullracing.message.PingMessage;
import greenbullracing.message.SwitchLaneMessage;
import greenbullracing.message.ThrottleMessage;
import greenbullracing.message.data.SwitchDirection;

import java.io.IOException;

public class KeimolaHardCodeDriver extends Driver {

	@Override
	public void receive(Message message) throws IOException {
		if(message instanceof CarPositionsMessage) {
			CarPositionsMessage cpm=(CarPositionsMessage)message;
			if(cpm.getGameTick()==0)
				upHandler.send(new ThrottleMessage(0.65d));
			else if(cpm.getCarPositions().get(0).getPiecePosition().getPieceIndex()==1)
				upHandler.send(new SwitchLaneMessage(SwitchDirection.RIGHT));
			else if(cpm.getCarPositions().get(0).getPiecePosition().getPieceIndex()==6)
				upHandler.send(new SwitchLaneMessage(SwitchDirection.LEFT));
//			else if(cpm.getCarPositions().get(0).getPiecePosition().getPieceIndex()==12)
//				upHandler.send(new SwitchLaneMessage(SwitchDirection.LEFT));
			else if(cpm.getCarPositions().get(0).getPiecePosition().getPieceIndex()==17)
				upHandler.send(new SwitchLaneMessage(SwitchDirection.RIGHT));
//			else if(cpm.getCarPositions().get(0).getPiecePosition().getPieceIndex()==24)
//				upHandler.send(new SwitchLaneMessage(SwitchDirection.RIGHT));
			else if(cpm.getCarPositions().get(0).getPiecePosition().getPieceIndex()==28)
				upHandler.send(new SwitchLaneMessage(SwitchDirection.LEFT));
//			else if(cpm.getCarPositions().get(0).getPiecePosition().getPieceIndex()==34)
//				upHandler.send(new SwitchLaneMessage(SwitchDirection.RIGHT));
			else
				upHandler.send(new PingMessage());
		}
	}

}
/*
0  {"length":100.0}
1  {"length":100.0}
2  {"length":100.0}
3  {"length":100.0,"switch":true}
4  {"radius":100,"angle":45.0}
5  {"radius":100,"angle":45.0}
6  {"radius":100,"angle":45.0}
7  {"radius":100,"angle":45.0}
8  {"radius":200,"angle":22.5,"switch":true}
9  {"length":100.0}
10 {"length":100.0}
11 {"radius":200,"angle":-22.5}
12 {"length":100.0}
13 {"length":100.0,"switch":true}
14 {"radius":100,"angle":-45.0}
15 {"radius":100,"angle":-45.0}
16 {"radius":100,"angle":-45.0}
17 {"radius":100,"angle":-45.0}
18 {"length":100.0,"switch":true}
19 {"radius":100,"angle":45.0}
20 {"radius":100,"angle":45.0}
21 {"radius":100,"angle":45.0}
22 {"radius":100,"angle":45.0}
23 {"radius":200,"angle":22.5}
24 {"radius":200,"angle":-22.5}
25 {"length":100.0,"switch":true}
26 {"radius":100,"angle":45.0}
27 {"radius":100,"angle":45.0}
28 {"length":62.0}
29 {"radius":100,"angle":-45.0,"switch":true}
30 {"radius":100,"angle":-45.0}
31 {"radius":100,"angle":45.0}
32 {"radius":100,"angle":45.0}
33 {"radius":100,"angle":45.0}
34 {"radius":100,"angle":45.0}
35 {"length":100.0,"switch":true}
36 {"length":100.0}
37 {"length":100.0}
38 {"length":100.0}
39 {"length":90.0}
*/
