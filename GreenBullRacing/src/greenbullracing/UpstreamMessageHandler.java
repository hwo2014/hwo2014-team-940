package greenbullracing;

import java.io.IOException;

public interface UpstreamMessageHandler {

//	public DownstreamMessageHandler getMessageListener();
//	public void setMessageListener(DownstreamMessageHandler messageListener);
	public void send(Message message) throws IOException;

}
