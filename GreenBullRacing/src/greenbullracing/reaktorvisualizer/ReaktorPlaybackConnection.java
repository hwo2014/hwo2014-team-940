package greenbullracing.reaktorvisualizer;

import greenbullracing.Connection;
import greenbullracing.DownstreamMessageHandler;
import greenbullracing.Message;
import greenbullracing.message.CarPositionsMessage;
import greenbullracing.message.JoinMessage;
import greenbullracing.message.YourCarMessage;
import greenbullracing.message.data.CarId;
import greenbullracing.message.data.CarPosition;
import greenbullracing.message.data.JoinData;
import greenbullracing.reaktorvisualizer.message.FullCarPositionsMessage;
import greenbullracing.reaktorvisualizer.message.GameInitMessage;
import greenbullracing.reaktorvisualizer.message.ReaktorVisualizerMessage;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.zip.GZIPInputStream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility;
import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.introspect.VisibilityChecker;

public class ReaktorPlaybackConnection implements Runnable, Connection {
	private static final Logger log=LoggerFactory.getLogger(ReaktorPlaybackConnection.class);

	private static final ObjectMapper objectMapper=new ObjectMapper();

	static {
		objectMapper.setVisibilityChecker(VisibilityChecker.Std.defaultInstance().withFieldVisibility(Visibility.ANY));
		// production
//		objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES,false);
		// development
		objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES,true);
	}

	private DownstreamMessageHandler downHandler;
	private String playbackName;
	private List<Message> readMessages;

	@SuppressWarnings("unused")
	private ReaktorPlaybackConnection() {}

	public ReaktorPlaybackConnection(String playbackName) {
		this.playbackName=playbackName;
	}

	public void connect() throws IOException {
		log.debug("Connect to {}",playbackName);
		if(isGzipFile(playbackName))
			try(BufferedReader bufferedReader=new BufferedReader(new InputStreamReader(new GZIPInputStream(new FileInputStream(playbackName)),"utf-8"))) {
				readMessages=objectMapper.readValue( bufferedReader,new TypeReference<List<ReaktorVisualizerMessage>>(){});
			}
		else
			try(BufferedReader bufferedReader=new BufferedReader(new InputStreamReader(new FileInputStream(playbackName),"utf-8"))) {
				readMessages=objectMapper.readValue( bufferedReader,new TypeReference<List<ReaktorVisualizerMessage>>(){});
			}
	}

	protected boolean isGzipFile(String fileName) throws IOException {
		try(FileInputStream fis=new FileInputStream(fileName)) {
			if(fis.read()==0x1f && fis.read()==0x8b) {
				log.debug("the file has been gzippped");
				return true;
			}
		}
		log.debug("the file is plain text");
		return false;
	}

	public void run() {
		try {
			for(Message message: readMessages) {
				if(message instanceof FullCarPositionsMessage) {
					// FullCarPositions and CarPositions are a bit different...
					FullCarPositionsMessage fcpm=(FullCarPositionsMessage)message;
					CarPositionsMessage cpm=new CarPositionsMessage();
					cpm.setCarPositions(new ArrayList<CarPosition>(fcpm.getCarPositions()));
					cpm.setGameId(fcpm.getGameId());
					cpm.setGameTick(fcpm.getGameTick());
					// send both messages, not sure if this is what we want
					downHandler.receive(message);
					message=cpm;
				} else if(message instanceof GameInitMessage) {
					// generate missing messages
					CarId myCarId=((GameInitMessage)message).getGameInitData().getRace().getCars().get(0).getCarId();
					JoinData joinData=new JoinData();
					joinData.setKey("FAKEHAHAHA");
					joinData.setName(myCarId.getName());
					JoinMessage joinMessage=new JoinMessage();
					joinMessage.setData(joinData);
					downHandler.receive(joinMessage);
					YourCarMessage yourCarMessage=new YourCarMessage();
					yourCarMessage.setCarId(myCarId);
					downHandler.receive(yourCarMessage);
				}
				downHandler.receive(message);
			}
		} catch(IOException e) {
			log.error("handling failed",e);
		}
		log.debug("Done.");
	}

	// UpstreamMessageHandler
	@Override
	public void send(Message message) throws JsonGenerationException, JsonMappingException, IOException {
		// ignore, we are just a playback
		log.trace("! {}",objectMapper.writerWithType(message.getClass()).writeValueAsString(message));
	}

	// DownstreamMessageSender
	@Override
	public void setDownstreamMessageHandler(DownstreamMessageHandler downHandler) {
		this.downHandler=downHandler;
	}

}
