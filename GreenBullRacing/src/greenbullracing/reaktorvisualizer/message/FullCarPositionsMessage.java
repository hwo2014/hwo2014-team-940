package greenbullracing.reaktorvisualizer.message;

import greenbullracing.reaktorvisualizer.message.data.FullCarPosition;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

//{
//	"msgType": "fullCarPositions",
//	"data": [
//		{
//			"id": {
//				"name": "JustDoIt",
//				"color": "red"
//			},
//			"coordinates": {
//				"x": 43.99999999999996,
//				"y": -310.00000000000006
//			},
//			"angle": 90,
//			"angleOffset": 0,
//			"piecePosition": {
//				"pieceIndex": 0,
//				"inPieceDistance": 0,
//				"lane": {
//					"startLaneIndex": 0,
//					"endLaneIndex": 0
//				},
//				"lap": 0
//			}
//		}
//	],
//	"gameId": "1be40c05-99ea-43f4-9b68-7b903101e28a"
//},
//{
//  "msgType": "fullCarPositions",
//  "data": [
//    {
//      "id": {
//        "name": "JustDoIt",
//        "color": "red"
//      },
//      "coordinates": {
//        "x": 44.19999999999996,
//        "y": -310.00000000000006
//      },
//      "angle": 90,
//      "angleOffset": 0,
//      "piecePosition": {
//        "pieceIndex": 0,
//        "inPieceDistance": 0.2,
//        "lane": {
//          "startLaneIndex": 0,
//          "endLaneIndex": 0
//        },
//        "lap": 0
//      }
//    }
//  ],
//  "gameId": "1be40c05-99ea-43f4-9b68-7b903101e28a",
//  "gameTick": 1
//},
//{
//  "msgType": "carVelocities",
//  "data": [
//    {
//      "x": -0.17879933272011206,
//      "y": -0.08961472322583308
//    }
//  ],
//  "gameId": "1be40c05-99ea-43f4-9b68-7b903101e28a",
//  "gameTick": 1
//},
public class FullCarPositionsMessage implements ReaktorVisualizerMessage {

	@JsonProperty("data")
	protected List<FullCarPosition> carPositions;
	protected int gameTick;
	protected String gameId;

	public List<FullCarPosition> getCarPositions() {
		return carPositions;
	}
	public void setCarPositions(List<FullCarPosition> carPositions) {
		this.carPositions=carPositions;
	}
	public int getGameTick() {
		return gameTick;
	}
	public void setGameTick(int gameTick) {
		this.gameTick=gameTick;
	}
	public String getGameId() {
		return gameId;
	}
	public void setGameId(String gameId) {
		this.gameId=gameId;
	}

}
