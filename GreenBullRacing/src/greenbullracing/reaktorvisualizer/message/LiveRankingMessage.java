package greenbullracing.reaktorvisualizer.message;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

import greenbullracing.message.data.CarId;

//{
//  "msgType": "liveRanking",
//  "data": [
//    {
//      "name": "JustDoIt",
//      "color": "red"
//    }
//  ],
//  "gameId": "1be40c05-99ea-43f4-9b68-7b903101e28a",
//  "gameTick": 1
//},
public class LiveRankingMessage implements ReaktorVisualizerMessage {

	@JsonProperty("data")
	protected List<CarId> ranking;
	protected String gameId;
	protected int gameTick;

	public List<CarId> getRanking() {
		return ranking;
	}
	public void setRanking(List<CarId> ranking) {
		this.ranking=ranking;
	}
	public String getGameId() {
		return gameId;
	}
	public void setGameId(String gameId) {
		this.gameId=gameId;
	}
	public int getGameTick() {
		return gameTick;
	}
	public void setGameTick(int gameTick) {
		this.gameTick=gameTick;
	}

}
