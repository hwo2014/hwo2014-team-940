package greenbullracing.reaktorvisualizer.message.data;

import greenbullracing.message.data.CarId;

public class Bumpers {

	protected CarId bumper;
	protected CarId bumpee;

	public CarId getBumper() {
		return bumper;
	}
	public void setBumper(CarId bumper) {
		this.bumper=bumper;
	}
	public CarId getBumpee() {
		return bumpee;
	}
	public void setBumpee(CarId bumpee) {
		this.bumpee=bumpee;
	}

}
