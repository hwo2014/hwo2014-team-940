package greenbullracing.reaktorvisualizer.message.data;

import greenbullracing.message.data.CarPosition;

public class FullCarPosition extends CarPosition {

	protected Vector2 coordinates;
	protected double angleOffset;

	public Vector2 getCoordinates() {
		return coordinates;
	}
	public void setCoordinates(Vector2 coordinates) {
		this.coordinates=coordinates;
	}
	public double getAngleOffset() {
		return angleOffset;
	}
	public void setAngleOffset(double angleOffset) {
		this.angleOffset=angleOffset;
	}

}
