package greenbullracing.reaktorvisualizer.message;

import greenbullracing.reaktorvisualizer.message.data.Bumpers;

import com.fasterxml.jackson.annotation.JsonProperty;

//{
//  "msgType": "bump",
//  "data": {
//    "bumper": {
//      "name": "levyseppahitsaaja8",
//      "color": "lightred"
//    },
//    "bumpee": {
//      "name": "levyseppahitsaaja12",
//      "color": "lightorange"
//    }
//  },
//  "gameId": "586456cb-1ded-4de1-a126-174be3961e90",
//  "gameTick": 1457
//},
public class BumpMessage implements ReaktorVisualizerMessage {

	@JsonProperty("data")
	protected Bumpers bumpers;
	protected String gameId;
	protected int gameTick;

	public Bumpers getBumpers() {
		return bumpers;
	}
	public void setBumpers(Bumpers bumpers) {
		this.bumpers=bumpers;
	}
	public String getGameId() {
		return gameId;
	}
	public void setGameId(String gameId) {
		this.gameId=gameId;
	}
	public int getGameTick() {
		return gameTick;
	}
	public void setGameTick(int gameTick) {
		this.gameTick=gameTick;
	}

}
