package greenbullracing.reaktorvisualizer.message;

import java.util.List;

import greenbullracing.reaktorvisualizer.message.data.Vector2;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;

//{
//  "msgType": "carVelocities",
//  "data": [
//    {
//      "x": 0,
//      "y": 0
//    }
//  ],
//  "gameId": "1be40c05-99ea-43f4-9b68-7b903101e28a"
//},
//{
//  "msgType": "carVelocities",
//  "data": [
//    {
//      "x": -0.17879933272011206,
//      "y": -0.08961472322583308
//    }
//  ],
//  "gameId": "1be40c05-99ea-43f4-9b68-7b903101e28a",
//  "gameTick": 1
//},
public class CarVelocitiesMessage implements ReaktorVisualizerMessage {

	@JsonProperty("data")
	protected List<Vector2> velocities;
	protected String gameId;
	@JsonInclude(Include.NON_DEFAULT)
	protected int gameTick=-1;

	public List<Vector2> getVelocities() {
		return velocities;
	}
	public void setVelocities(List<Vector2> velocities) {
		this.velocities=velocities;
	}
	public String getGameId() {
		return gameId;
	}
	public void setGameId(String gameId) {
		this.gameId=gameId;
	}
	public int getGameTick() {
		return gameTick;
	}
	public void setGameTick(int gameTick) {
		this.gameTick=gameTick;
	}

}
