package greenbullracing.reaktorvisualizer.message;

//{
//	"msgType": "gameStartsIn",
//	"data": 0,
//	"gameId": "1be40c05-99ea-43f4-9b68-7b903101e28a"
//},
public class GameStartsInMessage implements ReaktorVisualizerMessage {

	protected Object data;
	protected String gameId;

	public Object getData() {
		return data;
	}
	public void setData(Object data) {
		this.data=data;
	}
	public String getGameId() {
		return gameId;
	}
	public void setGameId(String gameId) {
		this.gameId=gameId;
	}

}
