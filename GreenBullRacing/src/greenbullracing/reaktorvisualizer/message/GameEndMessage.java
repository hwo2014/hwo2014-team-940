package greenbullracing.reaktorvisualizer.message;

//{
//  "msgType": "gameEnd",
//  "data": {
//    "results": [
//      {
//        "car": {
//          "name": "JustDoIt",
//          "color": "red"
//        },
//        "result": {
//          "laps": 3,
//          "ticks": 1877,
//          "millis": 31284
//        }
//      }
//    ],
//    "bestLaps": [
//      {
//        "car": {
//          "name": "JustDoIt",
//          "color": "red"
//        },
//        "result": {
//          "lap": 1,
//          "ticks": 445,
//          "millis": 7417
//        }
//      }
//    ]
//  },
//  "gameId": "1be40c05-99ea-43f4-9b68-7b903101e28a"
//},
public class GameEndMessage extends greenbullracing.message.GameEndMessage implements ReaktorVisualizerMessage {
}
