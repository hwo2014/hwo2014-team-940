package greenbullracing;

import greenbullracing.message.CarPositionsMessage;
import greenbullracing.message.CrashMessage;
import greenbullracing.message.DNFMessage;
import greenbullracing.message.FinishMessage;
import greenbullracing.message.GameEndMessage;
import greenbullracing.message.GameInitMessage;
import greenbullracing.message.GameStartMessage;
import greenbullracing.message.JoinMessage;
import greenbullracing.message.LapFinishedMessage;
import greenbullracing.message.PingMessage;
import greenbullracing.message.SpawnMessage;
import greenbullracing.message.TournamentEndMessage;
import greenbullracing.message.TurboAvailableMessage;
import greenbullracing.message.TurboEndMessage;
import greenbullracing.message.TurboStartMessage;
import greenbullracing.message.YourCarMessage;
import greenbullracing.message.data.CarId;
import greenbullracing.message.data.CarPosition;

import java.io.IOException;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class OnDriver extends Driver {
	@SuppressWarnings("unused")
	private static final Logger log=LoggerFactory.getLogger(OnDriver.class);

	protected int serverGameTick=-1;
	protected CarId myCarId;

	protected CarPosition getCarPosition(List<CarPosition> positions, CarId carId) {
		for(CarPosition position: positions) {
			if(position.getCarId().getColor().equals(carId.getColor()))
				return position;
		}
		return null;
	}

	@Override
	public void receive(Message message) throws IOException {
		Message returnMessage=handleOnMessage(message);
		if(returnMessage!=null)
			upHandler.send(returnMessage);
	}

	protected void updateTick(int gameTick) {
		if(serverGameTick<gameTick)
			serverGameTick=gameTick;
	}

	public Message handleOnMessage(Message message) throws IOException {
		if(message instanceof JoinMessage) {
			return onJoin((JoinMessage)message);
		} else if(message instanceof YourCarMessage) {
			return onYourCar((YourCarMessage)message);
		} else if(message instanceof GameInitMessage) {
			return onGameInit((GameInitMessage)message);
		} else if(message instanceof CarPositionsMessage) {
			return onCarPositions((CarPositionsMessage)message);
		} else if(message instanceof GameStartMessage) {
			return onGameStart((GameStartMessage)message);
		} else if(message instanceof GameEndMessage) {
			return onGameEnd((GameEndMessage)message);
		} else if(message instanceof TournamentEndMessage) {
			return onTournamentEnd((TournamentEndMessage)message);
		} else if(message instanceof CrashMessage) {
			return onCrash((CrashMessage)message);
		} else if(message instanceof SpawnMessage) {
			return onSpawn((SpawnMessage)message);
		} else if(message instanceof LapFinishedMessage) {
			return onLapFinished((LapFinishedMessage)message);
		} else if(message instanceof DNFMessage) {
			return onDNF((DNFMessage)message);
		} else if(message instanceof FinishMessage) {
			return onFinish((FinishMessage)message);
		} else if(message instanceof TurboAvailableMessage) {
			return onTurboAvailable((TurboAvailableMessage)message);
		} else if(message instanceof TurboStartMessage) {
			return onTurboStart((TurboStartMessage)message);
		} else if(message instanceof TurboEndMessage) {
			return onTurboEnd((TurboEndMessage)message);
		}
		return null;
	}

	protected Message onJoin(JoinMessage message) {
		return null;
	}

	protected Message onYourCar(YourCarMessage message) {
		myCarId=message.getCarId();
		return null;
	}

	protected Message onGameInit(GameInitMessage message) {
		serverGameTick=-1;
		return null;
	}

	protected Message onCarPositions(CarPositionsMessage message) {
		updateTick(message.getGameTick());
		if(message.getGameTick()>=0)
			return new PingMessage();
		return null;
	}

	protected Message onGameStart(GameStartMessage message) {
		updateTick(message.getGameTick());
		return new PingMessage();
	}

	protected Message onGameEnd(GameEndMessage message) {
		return null;
	}

	protected Message onTournamentEnd(TournamentEndMessage message) {
		return null;
	}

	protected Message onCrash(CrashMessage message) {
		updateTick(message.getGameTick());
		return null;
	}

	protected Message onSpawn(SpawnMessage message) {
		updateTick(message.getGameTick());
		return null;
	}

	protected Message onLapFinished(LapFinishedMessage message) {
		updateTick(message.getGameTick());
		return null;
	}

	protected Message onDNF(DNFMessage message) {
		updateTick(message.getGameTick());
		return null;
	}

	protected Message onFinish(FinishMessage message) {
		updateTick(message.getGameTick());
		return null;
	}

	protected Message onTurboAvailable(TurboAvailableMessage message) {
		updateTick(message.getGameTick());
		return null;
	}

	protected Message onTurboStart(TurboStartMessage message) {
		updateTick(message.getGameTick());
		return null;
	}

	protected Message onTurboEnd(TurboEndMessage message) {
		updateTick(message.getGameTick());
		return null;
	}

}
