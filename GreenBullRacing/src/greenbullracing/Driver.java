package greenbullracing;


import greenbullracing.track.Track;
import greenbullracing.route.Edge;
import greenbullracing.route.Graph;
import greenbullracing.route.Vertex;
import java.io.IOException;
import java.util.List;

public abstract class Driver implements DownstreamMessageHandler, UpstreamMessageSender {

	protected UpstreamMessageHandler upHandler;
	protected Graph graph;
	protected Track track;
	protected String myColor;
//	protected Statistics stats;

	@Override
	public abstract void receive(Message message) throws IOException;

	@Override
	public void setUpstreamMessageHandler(UpstreamMessageHandler upHandler) {
		this.upHandler=upHandler;
	}

}
