/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package greenbullracing.track;

import greenbullracing.message.CarPositionsMessage;
import greenbullracing.message.data.CarPosition;
import java.util.HashMap;
import java.util.List;

/**
 *
 * @author dogo
 */
public class Cars {
	HashMap<String, Car> cars = new HashMap<>();	
	Track track;

	public Cars(Track track) {
		this.track = track;
	}
	
	public void update(CarPositionsMessage cpm) {
		List<CarPosition> cps = cpm.getCarPositions();
		int gametick = cpm.getGameTick();
		if (gametick < 0) { gametick = 0; };
		for (CarPosition cp : cps) {
			if (!cars.containsKey(cp.getCarId().getColor())) {
				cars.put(cp.getCarId().getColor(), new Car(track));
			}
			
			cars.get(cp.getCarId().getColor()).update(cp, gametick);
		}
	}
	
	public Car getCar(String color) {
		return cars.get(color);
	}
}

