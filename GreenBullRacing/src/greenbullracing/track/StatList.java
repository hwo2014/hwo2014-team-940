/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package greenbullracing.track;

import java.util.List;

/**
 *
 * @author dogo
 */
public class StatList {
	private List<Double> speed;
	private List<Double> angle;
	private List<Double> acceleration;
	private List<Integer> pieceIndex;
	private List<Double> inPiecePosition;
	private List<Integer> endLaneIndex;
	private List<Integer> gameTick;

	private List<Integer> startLaneIndex;

	public List<Integer> getStartLaneIndex() {
		return startLaneIndex;
	}

	public List<Integer> getEndLaneIndex() {
		return endLaneIndex;
	}
	public List<Integer> getGameTick() {
		return gameTick;
	}

	public List<Double> getSpeed() {
		return speed;
	}

	public List<Double> getAngle() {
		return angle;
	}

	public List<Double> getAcceleration() {
		return acceleration;
	}

	public List<Integer> getPieceIndex() {
		return pieceIndex;
	}

	public List<Double> getInPiecePosition() {
		return inPiecePosition;
	}

	public StatList(List<Double> speed, List<Double> angle, List<Double> acceleration, List<Integer> pieceIndex, List<Double> inPiecePosition, List<Integer> startLaneIndex, List<Integer> endLaneIndex, List<Integer> gameTick) {
		this.speed = speed;
		this.angle = angle;
		this.acceleration = acceleration;
		this.pieceIndex = pieceIndex;
		this.inPiecePosition = inPiecePosition;
		this.startLaneIndex = startLaneIndex;
		this.endLaneIndex = endLaneIndex;
		this.gameTick = gameTick;

	}
}
