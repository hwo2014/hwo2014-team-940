package greenbullracing.track;

import greenbullracing.route.Edge;

import java.util.List;

public class Track {
//	private HashMap<TrackPiece, ArrayList<Double>> lengthList;
	private double[][][] lanelengths;
	
	public Track(List<Edge> edgelist, int piececount, int lanecount) {
		createPieceList(edgelist, piececount, lanecount);
	}

	private void createPieceList(List<Edge> edgelist, int piececount, int lanecount) {
		lanelengths = new double[piececount][lanecount][lanecount];
		for (Edge e : edgelist) {
			lanelengths[e.getPieceIndex()][e.getStartLane()][e.getEndLane()] = e.getWeight();
		}
	}
	
	public double getLaneLength(int pieceIndex, int startLane, int endLane) {
		return lanelengths[pieceIndex][startLane][endLane];
	}
}