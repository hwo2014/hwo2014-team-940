/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package greenbullracing.track;

import greenbullracing.message.data.CarPosition;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author dogo
 */
public class Car {
    
	private double lastinPiecePosition = 0.0;
//	private double lastAngle = 0.0;
	private int lasttick = 0;
        private int lasttickdelta = 1;
	private int lastpieceIndex = 0;
	private int lastlaneIndex = 0;
        private double lastSpeed = 0.0;
//	private int speed;
	private static final int SAMPLE_COUNT = 262144; // each tick is one sample, 262144 should suffice
        private double MAX_SPEED = 10.0;
	private List<Double> speed = new ArrayList<>();
	private List<Double> inPiecePosition = new ArrayList<>();
	private List<Double> angle = new ArrayList<>();
	private List<Double> acceleration = new ArrayList<>();
	private List<Integer> pieceIndex = new ArrayList<>();
	private List<Integer> startLaneIndex = new ArrayList<>();
	private List<Integer> endLaneIndex = new ArrayList<>();
	private List<Integer> gameTick = new ArrayList();
//	private double currentspeedEMA = 0;
//	private double[] ema = { -1, -1 };
//	private int gameTick = 0;
	private Track track;
        private double decInitSpeed = 0.0;
        private double accSlope = 0.2;
        private double decSlope = -0.2;
        private double decAtFull = -0.2;
        private double lastAcceleration = 0.0;

	public Car(Track track) {
		this.track = track;

		for (int i = 0; i < SAMPLE_COUNT; i++) {
			speed.add(0.0);
			angle.add(0.0);
			acceleration.add(0.0);
			pieceIndex.add(0);
			startLaneIndex.add(0);
			endLaneIndex.add(0);
			inPiecePosition.add(-1.0);
			gameTick.add(0);
		}
		inPiecePosition.set(0, 0.0);
	}
	public void update(CarPosition cpm, int gametick) {

		int tickdelta = gametick - lasttick;
                if (tickdelta == 0) {
                    tickdelta = lasttickdelta;
                } else {
                    lasttickdelta = tickdelta;
                }
		int currentpieceIndex = cpm.getPiecePosition().getPieceIndex();
		double currentinPiecePosition = cpm.getPiecePosition().getInPieceDistance();
		
		if (currentpieceIndex == lastpieceIndex) {
                    lastSpeed = (currentinPiecePosition - lastinPiecePosition) / tickdelta;
		} else {
                    lastSpeed = (currentinPiecePosition + (track.getLaneLength(lastpieceIndex, startLaneIndex.get((gametick + SAMPLE_COUNT - 1) % SAMPLE_COUNT), lastlaneIndex) - lastinPiecePosition) / tickdelta);
		}
		speed.set(gametick % SAMPLE_COUNT, lastSpeed);

                lastAcceleration = (speed.get(gametick % SAMPLE_COUNT) - speed.get((gametick + SAMPLE_COUNT - 1) % SAMPLE_COUNT));
		angle.set(gametick % SAMPLE_COUNT, cpm.getAngle());
		acceleration.set(gametick % SAMPLE_COUNT, lastAcceleration);
		pieceIndex.set(gametick % SAMPLE_COUNT, currentpieceIndex);
		inPiecePosition.set(gametick % SAMPLE_COUNT, currentinPiecePosition);
		lastlaneIndex = cpm.getPiecePosition().getLane().getEndLaneIndex();
		startLaneIndex.set(gametick % SAMPLE_COUNT, cpm.getPiecePosition().getLane().getStartLaneIndex());
		endLaneIndex.set(gametick % SAMPLE_COUNT, lastlaneIndex);
//		lastAngle = cpm.getAngle();
		lastinPiecePosition = currentinPiecePosition;
		lastpieceIndex = currentpieceIndex;
		gameTick.set(gametick % SAMPLE_COUNT, gametick);
		
		lasttick = gametick;

	}

	public StatList getStatLists() {
            return new StatList(speed, angle, acceleration, pieceIndex, inPiecePosition, startLaneIndex, endLaneIndex, gameTick);
	}
        
        public double getLastSpeed() {
            return lastSpeed;
        }
        
        public void setAccelerationSlope() {
            this.accSlope = lastSpeed;
        }
        
        public void setInitialSpeedForDecelerationEstimate() {
            this.decInitSpeed = lastSpeed;
        }
        
        public void estimateDecelerationSlope(int ticks) {
            //double deltaDec = ((lastSpeed - decInitSpeed) / ticks);
            decSlope = ((lastSpeed - decInitSpeed) / ticks) * (1.0 / ((decInitSpeed + lastSpeed) / 2));
            decAtFull = MAX_SPEED * decSlope;
        }
        
        public double getLastAcceleration() {
            return lastAcceleration;
        }
        
        public double getDecAtSpeed(double speed) {
            return decSlope * speed;
        }

    public double getMaxAcceleration() {
        return accSlope;
    }

    public double getDecSlope() {
        return decSlope;
    }
    
    public double getAccSlope() {
        return accSlope;
    }

    public double getDecAtFull() {
        return decAtFull;
    }
}
