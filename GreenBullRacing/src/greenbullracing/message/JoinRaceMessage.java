package greenbullracing.message;

import greenbullracing.ServerMessage;
import greenbullracing.message.data.AdvancedRaceData;
import greenbullracing.message.data.BotId;

import com.fasterxml.jackson.annotation.JsonProperty;

//{"msgType": "joinRace", "data": {
//  "botId": {
//    "name": "keke",
//    "key": "IVMNERKWEW"
//  },
//  "trackName": "hockenheimring",
//  "password": "schumi4ever",
//  "carCount": 3
//}}

public class JoinRaceMessage implements ServerMessage {

	@JsonProperty("data")
	protected AdvancedRaceData advancedRaceData;

	public JoinRaceMessage() {
	}

	public JoinRaceMessage(String botName, String botKey, int carCount, String trackName, String password) {
		advancedRaceData=new AdvancedRaceData();
		BotId botId=new BotId();
		botId.setName(botName);
		botId.setKey(botKey);
		advancedRaceData.setBotId(botId);
		advancedRaceData.setCarCount(carCount);
		advancedRaceData.setTrackName(trackName);
		advancedRaceData.setPassword(password);
	}

	public AdvancedRaceData getAdvancedRaceData() {
		return advancedRaceData;
	}
	public void setAdvancedRaceData(AdvancedRaceData advancedRaceData) {
		this.advancedRaceData=advancedRaceData;
	}

}
