package greenbullracing.message;

import greenbullracing.ServerMessage;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;

//{"msgType":"throttle","data":1.0}
//{"msgType":"throttle","data":1.0, "gameTick": 666}

public class ThrottleMessage implements ServerMessage {

	@JsonProperty("data")
	protected double throttle;
	@JsonInclude(Include.NON_DEFAULT)
	protected int gameTick=-1;

	public ThrottleMessage() {
	}

	public ThrottleMessage(double throttle) {
		this.throttle=throttle;
	}

	public ThrottleMessage(double throttle, int gameTick) {
		this.throttle=throttle;
		this.gameTick=gameTick;
	}

	public double getThrottle() {
		return throttle;
	}
	public void setThrottle(double throttle) {
		this.throttle=throttle;
	}
	public int getGameTick() {
		return gameTick;
	}
	public void setGameTick(int gameTick) {
		this.gameTick=gameTick;
	}

}
