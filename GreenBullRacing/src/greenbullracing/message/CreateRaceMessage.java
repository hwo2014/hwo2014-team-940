package greenbullracing.message;

import greenbullracing.ServerMessage;
import greenbullracing.message.data.AdvancedRaceData;
import greenbullracing.message.data.BotId;

import com.fasterxml.jackson.annotation.JsonProperty;

//{"msgType": "createRace", "data": {
//  "botId": {
//    "name": "schumacher",
//    "key": "UEWJBVNHDS"
//  },
//  "trackName": "hockenheimring",
//  "password": "schumi4ever",
//  "carCount": 3
//}}

public class CreateRaceMessage implements ServerMessage {

	@JsonProperty("data")
	protected AdvancedRaceData advancedRaceData;

	public CreateRaceMessage() {
	}

	public CreateRaceMessage(String botName, String botKey, int carCount, String trackName, String password) {
		advancedRaceData=new AdvancedRaceData();
		BotId botId=new BotId();
		botId.setName(botName);
		botId.setKey(botKey);
		advancedRaceData.setBotId(botId);
		advancedRaceData.setCarCount(carCount);
		advancedRaceData.setTrackName(trackName);
		advancedRaceData.setPassword(password);
	}

	public AdvancedRaceData getAdvancedRaceData() {
		return advancedRaceData;
	}
	public void setAdvancedRaceData(AdvancedRaceData advancedRaceData) {
		this.advancedRaceData=advancedRaceData;
	}

}
