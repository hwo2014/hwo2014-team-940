package greenbullracing.message;

import greenbullracing.ServerMessage;
import greenbullracing.message.data.SwitchDirection;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;

//{"msgType": "switchLane", "data": "Left"}
//{"msgType": "switchLane", "data": "Left", "gameTick": 666}

public class SwitchLaneMessage implements ServerMessage {

	@JsonProperty("data")
	protected SwitchDirection direction;
	@JsonInclude(Include.NON_DEFAULT)
	protected int gameTick=-1;

	public SwitchLaneMessage() {
	}

	public SwitchLaneMessage(SwitchDirection direction) {
		this.direction=direction;
	}

	public SwitchLaneMessage(SwitchDirection direction, int gameTick) {
		this.direction=direction;
		this.gameTick=gameTick;
	}

	public SwitchDirection getDirection() {
		return direction;
	}
	public void setDirection(SwitchDirection direction) {
		this.direction=direction;
	}
	public int getGameTick() {
		return gameTick;
	}
	public void setGameTick(int gameTick) {
		this.gameTick=gameTick;
	}

}
