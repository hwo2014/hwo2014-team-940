package greenbullracing.message;

import greenbullracing.ServerMessage;
import greenbullracing.message.data.JoinData;

//{"msgType": "join", "data": {"name": "Schumacher", "key": "UEWJBVNHDS"}}

public class JoinMessage implements ServerMessage {
	protected JoinData data;

	public JoinMessage() {
	}

	public JoinMessage(JoinData data) {
		this.data=data;
	}

	public JoinMessage(String name, String key) {
		data=new JoinData();
		data.setName(name);
		data.setKey(key);
	}

	public JoinData getData() {
		return data;
	}

	public void setData(JoinData joinData) {
		this.data=joinData;
	}


}
