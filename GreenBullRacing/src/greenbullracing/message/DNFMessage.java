package greenbullracing.message;

import greenbullracing.ServerMessage;
import greenbullracing.message.data.DNFData;

import com.fasterxml.jackson.annotation.JsonProperty;

//{"msgType":"dnf","data":{
//	"car":{
//		"name":"Rosberg",
//		"color":"blue"
//	},
//	"reason":"disconnected"
//},
//"gameId":"OIUHGERJWEOI",
//"gameTick":650
//}

public class DNFMessage implements ServerMessage {

	@JsonProperty("data")
	protected DNFData dnfData;
	protected String gameId;
	protected int gameTick;

	public DNFData getDnfData() {
		return dnfData;
	}
	public void setDnfData(DNFData dnfData) {
		this.dnfData=dnfData;
	}
	public String getGameId() {
		return gameId;
	}
	public void setGameId(String gameId) {
		this.gameId=gameId;
	}
	public int getGameTick() {
		return gameTick;
	}
	public void setGameTick(int gameTick) {
		this.gameTick=gameTick;
	}

}
