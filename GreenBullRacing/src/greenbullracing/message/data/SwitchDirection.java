package greenbullracing.message.data;

import com.fasterxml.jackson.annotation.JsonValue;

public enum SwitchDirection {
	LEFT("Left")
	,RIGHT("Right");

	private String jsonName;

	private SwitchDirection(String jsonName) {
		this.jsonName=jsonName;
	}

	@JsonValue
	public String getJsonName() {
		return jsonName;
	}

}
