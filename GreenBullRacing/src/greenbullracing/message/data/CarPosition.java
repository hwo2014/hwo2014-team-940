package greenbullracing.message.data;

import com.fasterxml.jackson.annotation.JsonProperty;

public class CarPosition {

	@JsonProperty("id")
	protected CarId carId;
	protected double angle;
	protected PiecePosition piecePosition;
	protected int prevCommandTick=-1;

	public CarId getCarId() {
		return carId;
	}
	public void setCarId(CarId carId) {
		this.carId=carId;
	}
	public double getAngle() {
		return angle;
	}
	public void setAngle(double angle) {
		this.angle=angle;
	}
	public PiecePosition getPiecePosition() {
		return piecePosition;
	}
	public void setPiecePosition(PiecePosition piecePosition) {
		this.piecePosition=piecePosition;
	}
	public int getPrevCommandTick() {
		return prevCommandTick;
	}
	public void setPrevCommandTick(int prevCommandTick) {
		this.prevCommandTick=prevCommandTick;
	}

}
