package greenbullracing.message.data;

public class CarDimensions {

	protected double length;
	protected double width;
	protected double guideFlagPosition;

	public double getLength() {
		return length;
	}
	public void setLength(double length) {
		this.length=length;
	}
	public double getWidth() {
		return width;
	}
	public void setWidth(double width) {
		this.width=width;
	}
	public double getGuideFlagPosition() {
		return guideFlagPosition;
	}
	public void setGuideFlagPosition(double guideFlagPosition) {
		this.guideFlagPosition=guideFlagPosition;
	}

}
