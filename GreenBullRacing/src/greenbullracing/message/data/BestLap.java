package greenbullracing.message.data;

import com.fasterxml.jackson.annotation.JsonProperty;

public class BestLap {

	@JsonProperty("car")
	protected CarId carId;
	protected LapTime result;

	public CarId getCarId() {
		return carId;
	}
	public void setCarId(CarId carId) {
		this.carId=carId;
	}
	public LapTime getResult() {
		return result;
	}
	public void setResult(LapTime result) {
		this.result=result;
	}

}
