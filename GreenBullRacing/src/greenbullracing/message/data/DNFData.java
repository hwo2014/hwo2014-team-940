package greenbullracing.message.data;

import com.fasterxml.jackson.annotation.JsonProperty;

public class DNFData {

	@JsonProperty("car")
	protected CarId carId;
	protected String reason;

	public CarId getCarId() {
		return carId;
	}
	public void setCarId(CarId carId) {
		this.carId=carId;
	}
	public String getReason() {
		return reason;
	}
	public void setReason(String reason) {
		this.reason=reason;
	}

}
