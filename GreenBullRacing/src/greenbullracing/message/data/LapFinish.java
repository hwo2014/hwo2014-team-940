package greenbullracing.message.data;

import com.fasterxml.jackson.annotation.JsonProperty;

public class LapFinish {

	@JsonProperty("car")
	protected CarId carId;
	protected LapTime lapTime;
	protected RaceTime raceTime;
	protected Ranking ranking;

	public CarId getCarId() {
		return carId;
	}
	public void setCarId(CarId carId) {
		this.carId=carId;
	}
	public LapTime getLapTime() {
		return lapTime;
	}
	public void setLapTime(LapTime lapTime) {
		this.lapTime=lapTime;
	}
	public RaceTime getRaceTime() {
		return raceTime;
	}
	public void setRaceTime(RaceTime raceTime) {
		this.raceTime=raceTime;
	}
	public Ranking getRanking() {
		return ranking;
	}
	public void setRanking(Ranking ranking) {
		this.ranking=ranking;
	}

}
