package greenbullracing.message.data;

import java.util.List;

public class Race {

	protected Track track;
	protected List<CarDefinition> cars;
	protected RaceSession raceSession;

	public Track getTrack() {
		return track;
	}
	public void setTrack(Track track) {
		this.track=track;
	}
	public List<CarDefinition> getCars() {
		return cars;
	}
	public void setCars(List<CarDefinition> cars) {
		this.cars=cars;
	}
	public RaceSession getRaceSession() {
		return raceSession;
	}
	public void setRaceSession(RaceSession raceSession) {
		this.raceSession=raceSession;
	}

}
