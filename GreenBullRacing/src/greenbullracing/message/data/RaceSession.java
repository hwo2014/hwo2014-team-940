package greenbullracing.message.data;

import com.fasterxml.jackson.annotation.JsonProperty;

public class RaceSession {

	protected int laps=-1;
	@JsonProperty("maxLapTimeMs")
	protected long maxLapTimeMillis=-1L;
	@JsonProperty("durationMs")
	protected long durationMillis=-1L;
	protected boolean quickRace;

	public int getLaps() {
		return laps;
	}
	public void setLaps(int laps) {
		this.laps=laps;
	}
	public long getMaxLapTimeMillis() {
		return maxLapTimeMillis;
	}
	public void setMaxLapTimeMillis(long maxLapTimeMillis) {
		this.maxLapTimeMillis=maxLapTimeMillis;
	}
	public long getDurationMillis() {
		return durationMillis;
	}
	public void setDurationMillis(long durationMillis) {
		this.durationMillis=durationMillis;
	}
	public boolean isQuickRace() {
		return quickRace;
	}
	public void setQuickRace(boolean quickRace) {
		this.quickRace=quickRace;
	}

}
