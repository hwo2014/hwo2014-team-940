package greenbullracing.message.data;

import java.util.List;

public class Track {

	protected String id;
	protected String name;
	protected List<TrackPiece> pieces;
	protected List<TrackLane> lanes;
	protected StartingPoint startingPoint;

	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id=id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name=name;
	}
	public List<TrackPiece> getPieces() {
		return pieces;
	}
	public void setPieces(List<TrackPiece> pieces) {
		this.pieces=pieces;
	}
	public List<TrackLane> getLanes() {
		return lanes;
	}
	public void setLanes(List<TrackLane> lanes) {
		this.lanes=lanes;
	}
	public StartingPoint getStartingPoint() {
		return startingPoint;
	}
	public void setStartingPoint(StartingPoint startingPoint) {
		this.startingPoint=startingPoint;
	}

}
