package greenbullracing.message.data;

public class GameInitData {

	protected Race race;

	public Race getRace() {
		return race;
	}
	public void setRace(Race race) {
		this.race=race;
	}

}
