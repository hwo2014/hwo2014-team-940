package greenbullracing.message.data;

import java.util.List;

public class ResultData {

	protected List<RaceResult> results;
	protected List<BestLap> bestLaps;

	public List<RaceResult> getResults() {
		return results;
	}
	public void setResults(List<RaceResult> results) {
		this.results=results;
	}
	public List<BestLap> getBestLaps() {
		return bestLaps;
	}
	public void setBestLaps(List<BestLap> bestLaps) {
		this.bestLaps=bestLaps;
	}

}
