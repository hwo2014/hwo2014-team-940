package greenbullracing.message.data;

public class TrackLane {

	protected double distanceFromCenter;
	protected int index;

	public double getDistanceFromCenter() {
		return distanceFromCenter;
	}
	public void setDistanceFromCenter(double distanceFromCenter) {
		this.distanceFromCenter=distanceFromCenter;
	}
	public int getIndex() {
		return index;
	}
	public void setIndex(int index) {
		this.index=index;
	}
	@Override
	public String toString() {
		return super.toString()+"[index="+index+" distanceFromCenter="+distanceFromCenter+"]";
	}


}
