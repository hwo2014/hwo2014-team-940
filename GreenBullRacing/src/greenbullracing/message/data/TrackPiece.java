package greenbullracing.message.data;

import com.fasterxml.jackson.annotation.JsonProperty;

public class TrackPiece {

	protected double length;
	protected double angle;
	protected double radius;
	@JsonProperty("switch")
	protected boolean switchPresent;
	protected boolean bridge;

	public double getLength() {
		return length;
	}
	public void setLength(double length) {
		this.length=length;
	}
	public double getAngle() {
		return angle;
	}
	public void setAngle(double angle) {
		this.angle=angle;
	}
	public double getRadius() {
		return radius;
	}
	public void setRadius(double radius) {
		this.radius=radius;
	}
	public boolean isSwitchPresent() {
		return switchPresent;
	}
	public void setSwitchPresent(boolean switchPresent) {
		this.switchPresent=switchPresent;
	}
	public boolean isBridge() {
		return bridge;
	}
	public void setBridge(boolean bridge) {
		this.bridge=bridge;
	}

}
