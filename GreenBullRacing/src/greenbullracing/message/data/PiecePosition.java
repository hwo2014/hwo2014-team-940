package greenbullracing.message.data;

public class PiecePosition {

	protected int pieceIndex;
	protected double inPieceDistance;
	protected LaneSwitch lane;
	protected int lap;

	public int getPieceIndex() {
		return pieceIndex;
	}
	public void setPieceIndex(int pieceIndex) {
		this.pieceIndex=pieceIndex;
	}
	public double getInPieceDistance() {
		return inPieceDistance;
	}
	public void setInPieceDistance(double inPieceDistance) {
		this.inPieceDistance=inPieceDistance;
	}
	public LaneSwitch getLane() {
		return lane;
	}
	public void setLane(LaneSwitch lane) {
		this.lane=lane;
	}
	public int getLap() {
		return lap;
	}
	public void setLap(int lap) {
		this.lap=lap;
	}

}
