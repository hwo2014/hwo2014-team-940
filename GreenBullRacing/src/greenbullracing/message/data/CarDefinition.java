package greenbullracing.message.data;

import com.fasterxml.jackson.annotation.JsonProperty;

public class CarDefinition {

	@JsonProperty("id")
	protected CarId carId;
	protected CarDimensions dimensions;

	public CarId getCarId() {
		return carId;
	}
	public void setCarId(CarId carId) {
		this.carId=carId;
	}
	public CarDimensions getDimensions() {
		return dimensions;
	}
	public void setDimensions(CarDimensions dimensions) {
		this.dimensions=dimensions;
	}

}
