package greenbullracing.message.data;

import com.fasterxml.jackson.annotation.JsonProperty;

public class RaceResult {

	@JsonProperty("car")
	protected CarId carId;
	protected RaceTime result;

	public CarId getCarId() {
		return carId;
	}
	public void setCarId(CarId carId) {
		this.carId=carId;
	}
	public RaceTime getResult() {
		return result;
	}
	public void setResult(RaceTime result) {
		this.result=result;
	}

}
