package greenbullracing.message.data;

public class StartingPoint {

	protected Position position;
	protected double angle;

	public Position getPosition() {
		return position;
	}
	public void setPosition(Position position) {
		this.position=position;
	}
	public double getAngle() {
		return angle;
	}
	public void setAngle(double angle) {
		this.angle=angle;
	}

}
