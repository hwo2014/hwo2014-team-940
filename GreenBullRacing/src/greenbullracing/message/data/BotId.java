package greenbullracing.message.data;

public class BotId {

	protected String name;
	protected String key;

	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name=name;
	}
	public String getKey() {
		return key;
	}
	public void setKey(String key) {
		this.key=key;
	}

}
