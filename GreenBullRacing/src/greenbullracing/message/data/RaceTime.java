package greenbullracing.message.data;

// Allow for all three result types:
//  "result":{"laps":3,"ticks":1582,"millis":26366}
//  "result":{"reason":"race session timeout"}
//  "result":{"lap":1,"ticks":538,"millis":8966}

public class RaceTime {

	protected int laps=-1;
	protected int lap=-1;
	protected int ticks;
	protected long millis;
	protected String reason;

	public int getLaps() {
		return laps;
	}
	public void setLaps(int laps) {
		this.laps=laps;
	}
	public int getLap() {
		return lap;
	}
	public void setLap(int lap) {
		this.lap=lap;
	}
	public int getTicks() {
		return ticks;
	}
	public void setTicks(int ticks) {
		this.ticks=ticks;
	}
	public long getMillis() {
		return millis;
	}
	public void setMillis(long millis) {
		this.millis=millis;
	}
	public String getReason() {
		return reason;
	}
	public void setReason(String reason) {
		this.reason=reason;
	}

}
