package greenbullracing.message.data;

public class LaneSwitch {

	protected int startLaneIndex;
	protected int endLaneIndex;

	public int getStartLaneIndex() {
		return startLaneIndex;
	}
	public void setStartLaneIndex(int startLaneIndex) {
		this.startLaneIndex=startLaneIndex;
	}
	public int getEndLaneIndex() {
		return endLaneIndex;
	}
	public void setEndLaneIndex(int endLaneIndex) {
		this.endLaneIndex=endLaneIndex;
	}

}
