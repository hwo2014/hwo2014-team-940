package greenbullracing.message.data;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonInclude(Include.NON_NULL)
public class AdvancedRaceData {

	protected BotId botId;
	protected String trackName;
	protected String password;
	protected int carCount;

	public BotId getBotId() {
		return botId;
	}
	public void setBotId(BotId botId) {
		this.botId=botId;
	}
	public String getTrackName() {
		return trackName;
	}
	public void setTrackName(String trackName) {
		this.trackName=trackName;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password=password;
	}
	public int getCarCount() {
		return carCount;
	}
	public void setCarCount(int carCount) {
		this.carCount=carCount;
	}

}
