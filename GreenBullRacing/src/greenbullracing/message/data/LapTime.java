package greenbullracing.message.data;

public class LapTime {

	protected int lap;
	protected int ticks;
	protected long millis;
	protected String reason;

	public int getLap() {
		return lap;
	}
	public void setLap(int lap) {
		this.lap=lap;
	}
	public int getTicks() {
		return ticks;
	}
	public void setTicks(int ticks) {
		this.ticks=ticks;
	}
	public long getMillis() {
		return millis;
	}
	public void setMillis(long millis) {
		this.millis=millis;
	}
	public String getReason() {
		return reason;
	}
	public void setReason(String reason) {
		this.reason=reason;
	}

}
