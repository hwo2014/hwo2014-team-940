package greenbullracing.message.data;

import com.fasterxml.jackson.annotation.JsonProperty;

public class TurboData {

	@JsonProperty("turboDurationMilliseconds")
	protected double turboDurationMillis;
	protected int turboDurationTicks;
	protected double turboFactor;

	public double getTurboDurationMillis() {
		return turboDurationMillis;
	}
	public void setTurboDurationMillis(double turboDurationMillis) {
		this.turboDurationMillis=turboDurationMillis;
	}
	public int getTurboDurationTicks() {
		return turboDurationTicks;
	}
	public void setTurboDurationTicks(int turboDurationTicks) {
		this.turboDurationTicks=turboDurationTicks;
	}
	public double getTurboFactor() {
		return turboFactor;
	}
	public void setTurboFactor(double turboFactor) {
		this.turboFactor=turboFactor;
	}

}
