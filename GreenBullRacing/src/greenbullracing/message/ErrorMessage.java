package greenbullracing.message;

import greenbullracing.ServerMessage;

import com.fasterxml.jackson.annotation.JsonProperty;

//{"msgType":"error","data":"Invalid botname: y[x]"}

public class ErrorMessage implements ServerMessage{

	@JsonProperty("data")
	protected String errorText;

	public String getErrorText() {
		return errorText;
	}
	public void setErrorText(String errorText) {
		this.errorText=errorText;
	}

}
