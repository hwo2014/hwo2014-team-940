package greenbullracing.message;

import greenbullracing.ServerMessage;
import greenbullracing.message.data.ResultData;

import com.fasterxml.jackson.annotation.JsonProperty;

//{"msgType":"gameEnd","data":{
//	"results":[
//		{
//			"car":{
//				"name":"levyseppahitsaaja1",
//				"color":"red"
//			},
//			"result":{
//				"laps":3,
//				"ticks":1604,
//				"millis":26734
//			}
//		},{
//			"car":{"name":"levyseppahitsaaja3","color":"yellow"},"result":{"laps":3,"ticks":1582,"millis":26366}
//		},{
//			"car":{"name":"levyseppahitsaaja2","color":"blue"},"result":{"reason":"race session timeout"}
//		}
//	],
//	"bestLaps":[
//		{
//			"car":{"name":"levyseppahitsaaja1","color":"red"},
//			"result":{"lap":1,"ticks":518,"millis":8634}
//		},{
//			"car":{"name":"levyseppahitsaaja3","color":"yellow"},
//			"result":{"lap":1,"ticks":518,"millis":8633}
//		},{
//			"car":{"name":"levyseppahitsaaja2","color":"blue"},
//			"result":{"reason":"race session timeout"}
//		}
//	]
//},
//"gameId":"1a1c45e9-72ae-4be0-8298-f6587d90ca95"
//}
/*
{"msgType":"gameEnd","data":{
	"results":[
		{
			"car":{"name":"Green Bull Racing","color":"purple"},
			"result":{"lap":0,"ticks":518,"millis":8633}
		},{
			"car":{"name":"Traffic-3","color":"red"},
			"result":{"lap":1,"ticks":538,"millis":8966}
		},{
			"car":{"name":"Traffic-4","color":"yellow"},
			"result":{"lap":0,"ticks":574,"millis":9567}
		},{
			"car":{"name":"Traffic-2","color":"blue"},
			"result":{"lap":0,"ticks":586,"millis":9766}
		},{
			"car":{"name":"Traffic-5","color":"orange"},
			"result":{"lap":0,"ticks":603,"millis":10050}
		},{
			"car":{"name":"Traffic-1","color":"green"},
			"result":{"lap":0,"ticks":626,"millis":10433}
		}],
		"bestLaps":[
		{
			"car":{"name":"Green Bull Racing","color":"purple"},
			"result":{"lap":0,"ticks":518,"millis":8633}
		},{
			"car":{"name":"Traffic-3","color":"red"},
			"result":{"lap":1,"ticks":538,"millis":8966}
		},{
			"car":{"name":"Traffic-4","color":"yellow"},
			"result":{"lap":0,"ticks":574,"millis":9567}
		},{
			"car":{"name":"Traffic-2","color":"blue"},
			"result":{"lap":0,"ticks":586,"millis":9766}
		},{
			"car":{"name":"Traffic-5","color":"orange"},
			"result":{"lap":0,"ticks":603,"millis":10050}
		},{
			"car":{"name":"Traffic-1","color":"green"},
			"result":{"lap":0,"ticks":626,"millis":10433}
		}
	]
},"gameId":"9443ca8f-1a57-4a31-b24e-f37a6d5b18a0"}
 */
public class GameEndMessage implements ServerMessage {

	@JsonProperty("data")
	protected ResultData resultData;
	protected String gameId;

	public ResultData getResultData() {
		return resultData;
	}
	public void setResultData(ResultData resultData) {
		this.resultData=resultData;
	}
	public String getGameId() {
		return gameId;
	}
	public void setGameId(String gameId) {
		this.gameId=gameId;
	}

}
