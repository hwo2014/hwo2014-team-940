package greenbullracing.message;

import greenbullracing.ServerMessage;
import greenbullracing.message.data.CarId;

import com.fasterxml.jackson.annotation.JsonProperty;

//{"msgType":"crash","data":{"name":"Rosberg","color":"blue"},"gameId":"OIUHGERJWEOI","gameTick":3}

public class CrashMessage implements ServerMessage {

	@JsonProperty("data")
	protected CarId carId;
	protected String gameId;
	protected int gameTick;

	public CarId getCarId() {
		return carId;
	}
	public void setCarId(CarId carId) {
		this.carId=carId;
	}
	public String getGameId() {
		return gameId;
	}
	public void setGameId(String gameId) {
		this.gameId=gameId;
	}
	public int getGameTick() {
		return gameTick;
	}
	public void setGameTick(int gameTick) {
		this.gameTick=gameTick;
	}

}
