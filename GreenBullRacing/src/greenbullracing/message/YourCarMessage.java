package greenbullracing.message;

import greenbullracing.ServerMessage;
import greenbullracing.message.data.CarId;

import com.fasterxml.jackson.annotation.JsonProperty;

//{"msgType":"yourCar","data":{"name":"Levypeltiseppä","color":"red"},"gameId":"05057a8f-bdf7-4bd2-8e81-359a0af0f311"}

public class YourCarMessage implements ServerMessage {

	@JsonProperty("data")
	protected CarId carId;
	protected String gameId;

	public CarId getCarId() {
		return carId;
	}
	public void setCarId(CarId carId) {
		this.carId=carId;
	}
	public String getGameId() {
		return gameId;
	}
	public void setGameId(String gameId) {
		this.gameId=gameId;
	}

}
