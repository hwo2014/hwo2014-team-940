package greenbullracing.message;

import greenbullracing.ServerMessage;
import greenbullracing.message.data.CarPosition;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

//{"msgType": "carPositions", "data": [
//  {
//    "id": {
//      "name": "Schumacher",
//      "color": "red"
//    },
//    "angle": 0.0,
//    "piecePosition": {
//      "pieceIndex": 0,
//      "inPieceDistance": 0.0,
//      "lane": {
//        "startLaneIndex": 0,
//        "endLaneIndex": 0
//      },
//      "lap": 0
//    }
//  },
//  {
//    "id": {
//      "name": "Rosberg",
//      "color": "blue"
//    },
//    "angle": 45.0,
//    "piecePosition": {
//      "pieceIndex": 0,
//      "inPieceDistance": 20.0,
//      "lane": {
//        "startLaneIndex": 1,
//        "endLaneIndex": 1
//      },
//      "lap": 0
//    }
//  }
//], "gameId": "OIUHGERJWEOI", "gameTick": 0}

public class CarPositionsMessage implements ServerMessage {

	@JsonProperty("data")
	protected List<CarPosition> carPositions;
	protected String gameId;
	protected int gameTick=-1;

	public List<CarPosition> getCarPositions() {
		return carPositions;
	}
	public void setCarPositions(List<CarPosition> carPositions) {
		this.carPositions=carPositions;
	}
	public String getGameId() {
		return gameId;
	}
	public void setGameId(String gameId) {
		this.gameId=gameId;
	}
	public int getGameTick() {
		return gameTick;
	}
	public void setGameTick(int gameTick) {
		this.gameTick=gameTick;
	}

}
