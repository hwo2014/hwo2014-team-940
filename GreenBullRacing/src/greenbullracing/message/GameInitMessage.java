package greenbullracing.message;

import greenbullracing.ServerMessage;
import greenbullracing.message.data.GameInitData;

import com.fasterxml.jackson.annotation.JsonProperty;

//{"msgType":"gameInit","data":{
//	"race":{
//		"track":{
//			"id":"keimola",
//			"name":"Keimola",
//			"pieces":[
////				{"length:100.0,"bridge":true},
//				{"length":100.0},
//				{"length":100.0},
//				{"length":100.0},
//				{"length":100.0,"switch":true},
//				{"radius":100,"angle":45.0},
//				{"radius":100,"angle":45.0},
//				{"radius":100,"angle":45.0},
//				{"radius":100,"angle":45.0},
//				{"radius":200,"angle":22.5,"switch":true},
//				{"length":100.0},
//				{"length":100.0},
//				{"radius":200,"angle":-22.5},
//				{"length":100.0},
//				{"length":100.0,"switch":true},
//				{"radius":100,"angle":-45.0},
//				{"radius":100,"angle":-45.0},
//				{"radius":100,"angle":-45.0},
//				{"radius":100,"angle":-45.0},
//				{"length":100.0,"switch":true},
//				{"radius":100,"angle":45.0},
//				{"radius":100,"angle":45.0},
//				{"radius":100,"angle":45.0},
//				{"radius":100,"angle":45.0},
//				{"radius":200,"angle":22.5},
//				{"radius":200,"angle":-22.5},
//				{"length":100.0,"switch":true},
//				{"radius":100,"angle":45.0},
//				{"radius":100,"angle":45.0},
//				{"length":62.0},
//				{"radius":100,"angle":-45.0,"switch":true},
//				{"radius":100,"angle":-45.0},
//				{"radius":100,"angle":45.0},
//				{"radius":100,"angle":45.0},
//				{"radius":100,"angle":45.0},
//				{"radius":100,"angle":45.0},
//				{"length":100.0,"switch":true},
//				{"length":100.0},
//				{"length":100.0},
//				{"length":100.0},
//				{"length":90.0}
//			],
//			"lanes":[
//				{
//					"distanceFromCenter":-10,
//					"index":0
//				},{
//					"distanceFromCenter":10,
//					"index":1
//				}
//			],
//			"startingPoint":{
//				"position":{
//					"x":-300.0,
//					"y":-44.0
//				},
//				"angle":90.0
//			}
//		},
//		"cars":[
//			{
//				"id":{
//					"name":"Levypeltiseppä",
//					"color":"red"
//				},
//				"dimensions":{
//					"length":40.0,
//					"width":20.0,
//					"guideFlagPosition":10.0
//				}
//			}
//		],
//		"raceSession":{
//			"laps":3,
//			"maxLapTimeMs":60000,
//			"quickRace":true
//		}
//	}
//},
//"gameId":"297aa73f-477f-4d81-a9f8-d024b06078e1"
//}

public class GameInitMessage implements ServerMessage {

	@JsonProperty("data")
	protected GameInitData gameInitData;
	protected String gameId;

	public GameInitData getGameInitData() {
		return gameInitData;
	}
	public void setGameInitData(GameInitData gameInitData) {
		this.gameInitData=gameInitData;
	}
	public String getGameId() {
		return gameId;
	}
	public void setGameId(String gameId) {
		this.gameId=gameId;
	}

}
