package greenbullracing.message;

import com.fasterxml.jackson.annotation.JsonProperty;

import greenbullracing.ServerMessage;
import greenbullracing.message.data.TurboData;

// {"msgType":"turboAvailable","data":{"turboDurationMilliseconds":500.0,"turboFactor":3.0},"gameId":"cee9a988-cf41-4c43-a478-976f9f9c1c0d","gameTick":601}

public class TurboAvailableMessage implements ServerMessage {

	@JsonProperty("data")
	protected TurboData turboData;
	protected String gameId;
	protected int gameTick;

	public TurboData getTurboData() {
		return turboData;
	}
	public void setTurboData(TurboData turboData) {
		this.turboData=turboData;
	}
	public String getGameId() {
		return gameId;
	}
	public void setGameId(String gameId) {
		this.gameId=gameId;
	}
	public int getGameTick() {
		return gameTick;
	}
	public void setGameTick(int gameTick) {
		this.gameTick=gameTick;
	}

}
