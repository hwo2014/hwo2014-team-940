package greenbullracing.message;

import greenbullracing.ServerMessage;

//{"msgType":"tournamentEnd","data":null,"gameId":"8ff10961-0e50-4328-9567-d4cfddce1e37"}

public class TournamentEndMessage implements ServerMessage {

	protected Object data;
	protected String gameId;

	public Object getData() {
		return data;
	}
	public void setData(Object data) {
		this.data=data;
	}
	public String getGameId() {
		return gameId;
	}
	public void setGameId(String gameId) {
		this.gameId=gameId;
	}

}
