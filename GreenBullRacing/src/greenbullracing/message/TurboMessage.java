package greenbullracing.message;

import com.fasterxml.jackson.annotation.JsonProperty;

import greenbullracing.ServerMessage;

public class TurboMessage implements ServerMessage {

	@JsonProperty("data")
	protected String message="...and beyond!";

	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message=message;
	}

}
