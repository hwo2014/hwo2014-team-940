package greenbullracing.message;

import greenbullracing.ServerMessage;

//{"msgType":"gameStart","data":null,"gameId":"c5e83706-2259-49e6-9216-f029f5448bfe","gameTick":0}

public class GameStartMessage implements ServerMessage {

	protected Object data;
	protected String gameId;
	protected int gameTick;

	public Object getData() {
		return data;
	}
	public void setData(Object data) {
		this.data=data;
	}
	public String getGameId() {
		return gameId;
	}
	public void setGameId(String gameId) {
		this.gameId=gameId;
	}
	public int getGameTick() {
		return gameTick;
	}
	public void setGameTick(int gameTick) {
		this.gameTick=gameTick;
	}

}
