package greenbullracing.message;

import greenbullracing.ServerMessage;
import greenbullracing.message.data.LapFinish;

import com.fasterxml.jackson.annotation.JsonProperty;

//{"msgType":"lapFinished","data":{
//	"car":{
//		"name":"Schumacher",
//		"color":"red"
//	},
//	"lapTime":{
//		"lap":1,
//		"ticks":666,
//		"millis":6660
//	},
//	"raceTime":{
//		"laps":1,
//		"ticks":666,
//		"millis":6660
//	},
//	"ranking":{
//		"overall":1,
//		"fastestLap":1
//	}
//},
//"gameId":"OIUHGERJWEOI",
//"gameTick":300
//}

public class LapFinishedMessage implements ServerMessage {

	@JsonProperty("data")
	protected LapFinish lapFinishData;
	protected String gameId;
	protected int gameTick;

	public LapFinish getLapFinishData() {
		return lapFinishData;
	}
	public void setLapFinishData(LapFinish lapFinishData) {
		this.lapFinishData=lapFinishData;
	}
	public String getGameId() {
		return gameId;
	}
	public void setGameId(String gameId) {
		this.gameId=gameId;
	}
	public int getGameTick() {
		return gameTick;
	}
	public void setGameTick(int gameTick) {
		this.gameTick=gameTick;
	}

}
