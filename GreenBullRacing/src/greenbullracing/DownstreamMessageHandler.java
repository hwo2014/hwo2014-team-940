package greenbullracing;

import java.io.IOException;

public interface DownstreamMessageHandler {

//	public UpstreamMessageHandler getMessageConnection();
//	public void setMessageConnection(UpstreamMessageHandler messageConnection);
	public void receive(Message message) throws IOException;

}
