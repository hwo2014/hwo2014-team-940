package greenbullracing;

public interface Connection extends UpstreamMessageHandler, DownstreamMessageSender {
}
