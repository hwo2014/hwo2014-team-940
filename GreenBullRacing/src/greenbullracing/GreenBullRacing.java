package greenbullracing;

import greenbullracing.message.CreateRaceMessage;
import greenbullracing.message.JoinMessage;
import greenbullracing.message.JoinRaceMessage;
import greenbullracing.reaktorvisualizer.ReaktorPlaybackConnection;

import java.io.IOException;
import java.net.InetSocketAddress;

import net.sourceforge.argparse4j.ArgumentParsers;
import net.sourceforge.argparse4j.annotation.Arg;
import net.sourceforge.argparse4j.impl.Arguments;
import net.sourceforge.argparse4j.inf.ArgumentParser;
import net.sourceforge.argparse4j.inf.ArgumentParserException;
import net.sourceforge.argparse4j.inf.Subparser;
import net.sourceforge.argparse4j.inf.Subparsers;

import org.slf4j.ILoggerFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.LoggerContext;

public class GreenBullRacing {
//	private static final Logger log=LoggerFactory.getLogger(GreenBullRacing.class);
	// special case for logging; do not instantiate before configuration options have set the level
	private static Logger log;

	private static final String DEFAULT_SERVER="testserver.helloworldopen.com";
	private static final int DEFAULT_PORT=8091;
	private static final String DEFAULT_KEY="arsc1fzWAXtJ4g";
	private static final String DEFAULT_NAME="Levyseppahitsaaja";
	private static final String DEFAULT_TRACK="keimola";
	private static final String DEFAULT_DRIVER="LaneSwitcher";

	static {
		// before anything starts, add a Logback shutdown hook
		Runtime.getRuntime().addShutdownHook(new Thread() {
			@Override
			public void run() {
				ILoggerFactory ilf=LoggerFactory.getILoggerFactory();
				if(ilf instanceof LoggerContext) {
					((LoggerContext)ilf).stop();
				}
			}
		});
	}

	public static enum CommandLineAction {
		QUICKRACE
		,CREATERACE
		,JOINRACE
		,REAKTORPLAYBACK
	}

	public static class RaceConnectionConfiguration {
		@Arg(dest="server")
		String server;
		@Arg(dest="port")
		int port;
		@Arg(dest="key")
		String key;
		@Arg(dest="name")
		String name;
		@Arg(dest="driver")
		String driver;
		@Arg(dest="action")
		CommandLineAction action;
		@Arg(dest="carcount")
		int carCount;
		@Arg(dest="trackname")
		String trackName;
		@Arg(dest="password")
		String password;
		@Arg(dest="loglevel")
		String logLevel;
		@Arg(dest="filename")
		String filename;

		public InetSocketAddress getRemoteAddress() {
			return new InetSocketAddress(server,port);
		}

		@Override
		public String toString() {
			StringBuilder sb=new StringBuilder();
			sb.append(super.toString());

			switch(action) {
				case QUICKRACE:
					return String.format("%s[server=%s port=%d key=%s name=%s action=%s]",super.toString(),server,port,key,name,action);
				case CREATERACE:
				case JOINRACE:
					return String.format("%s[server=%s port=%d key=%s name=%s action=%s carCount=%d trackName=%s password=%s]",super.toString(),server,port,key,name,action,carCount,trackName,password);
				case REAKTORPLAYBACK:
					return String.format("%s[filename=%s action=%s]",super.toString(),filename,action);
				default:
					break;
			}
			return super.toString();
		}
	}

	public static void main(String[] args) throws Exception {
		ArgumentParser parser=ArgumentParsers.newArgumentParser("GreenBullRacing",true,"-",null);
		parser.defaultHelp(true);
		parser.addArgument("-s","--server").type(String.class).setDefault(DEFAULT_SERVER).help("server to connect to");
		parser.addArgument("-p","--port").type(Integer.class).setDefault(DEFAULT_PORT).choices(Arguments.range(1,65535)).help("server port to connect to");
		parser.addArgument("-k","--key").type(String.class).setDefault(DEFAULT_KEY).help("Key for the team");
		parser.addArgument("-n","--name").type(String.class).setDefault(DEFAULT_NAME).help("The name of the bot");
		parser.addArgument("-d","--driver").type(String.class).setDefault(DEFAULT_DRIVER).help("The name of the driver bot");
		parser.addArgument("-l","--loglevel").type(String.class).choices("off","error","warn","info","debug","trace","all").help("Overrides logback.xml definition of root logging level");

		Subparsers subparsers=parser.addSubparsers().title("subcommands").help("Please use <action> --help for more details").metavar("<action>");

		subparsers.addParser("quickrace").aliases("qr").setDefault("action",CommandLineAction.QUICKRACE).help("A quick race against a single bot").description("Creates and joins a quick race against a bot provided by the server");

		Subparser createRaceParser=subparsers.addParser("createrace").aliases("cr").setDefault("action",CommandLineAction.CREATERACE).help("Create a new race in the server others can join");
		createRaceParser.addArgument("carcount").type(Integer.class).required(true).help("number of cars in the race");
		createRaceParser.addArgument("trackname").type(String.class).nargs("?").help("name of the track of the race");
		createRaceParser.addArgument("password").type(String.class).nargs("?").help("password for the race");

		Subparser joinRaceParser=subparsers.addParser("joinrace").aliases("jr").setDefault("action",CommandLineAction.JOINRACE).help("Join a race created by someone else on the server");
		joinRaceParser.addArgument("carcount").type(Integer.class).required(true).help("number of cars in the race");
		joinRaceParser.addArgument("trackname").type(String.class).nargs("?").help("name of the track of the race");
		joinRaceParser.addArgument("password").type(String.class).nargs("?").help("password for the race");

		Subparser reactorPlaybackParser=subparsers.addParser("reaktorplayback").aliases("rp").setDefault("action",CommandLineAction.REAKTORPLAYBACK).help("Replay a reaktor visualization playback file like as it was a non-interactive race");
		reactorPlaybackParser.addArgument("filename").type(String.class).required(true).help("playback file name");

		RaceConnectionConfiguration config=new RaceConnectionConfiguration();
		try {
			parser.parseArgs(args,config);
		} catch(ArgumentParserException e) {
			parser.handleError(e);
			System.exit(1);
		}

		if(config.logLevel!=null) {
			Level level=Level.ALL;
			switch(config.logLevel) {
				case "off": level=Level.OFF; break;
				case "error": level=Level.ERROR; break;
				case "warn": level=Level.WARN; break;
				case "info": level=Level.INFO; break;
				case "debug": level=Level.DEBUG; break;
				case "trace": level=Level.TRACE; break;
				case "all": level=Level.ALL; break;
			}
			((ch.qos.logback.classic.Logger)org.slf4j.LoggerFactory.getLogger(ch.qos.logback.classic.Logger.ROOT_LOGGER_NAME)).setLevel(level);
		}
		log=LoggerFactory.getLogger(GreenBullRacing.class);
		log.debug("arguments: {}",config);

		switch(config.action) {
			case CREATERACE:
			case JOINRACE:
			case QUICKRACE:
				TCPConnection tcpConnection=new TCPConnection(config.getRemoteAddress());
				attachMessagePipeline(tcpConnection,config);
				tcpConnection.connect();
				switch(config.action) {
					case CREATERACE:
						tcpConnection.send(new CreateRaceMessage(config.name,config.key,config.carCount,config.trackName,config.password));
						break;
					case JOINRACE:
						tcpConnection.send(new JoinRaceMessage(config.name,config.key,config.carCount,config.trackName,config.password));
						break;
					case QUICKRACE:
						tcpConnection.send(new JoinMessage(config.name,config.key));
						break;
					default:
						// never happens, fall through
				}
				new Thread(tcpConnection).start();
				break;
			case REAKTORPLAYBACK:
				ReaktorPlaybackConnection rpConnection=new ReaktorPlaybackConnection(config.filename);
				attachMessagePipeline(rpConnection,config);
				rpConnection.connect();
				new Thread(rpConnection).start();
				break;
			default:
				log.error("Should never happen, no action");
				break;
		}
	}

	private static Connection attachMessagePipeline(Connection connection, RaceConnectionConfiguration config) throws IOException {
		Driver driver=getDriver(config.driver);
		driver.setUpstreamMessageHandler(connection);
		connection.setDownstreamMessageHandler(driver);

		return connection;
	}

	private static Driver getDriver(String name) {
		try {
			return (Driver)Class.forName("greenbullracing.driver."+name+"Driver").newInstance();
		} catch(InstantiationException|IllegalAccessException|ClassNotFoundException e) {
			throw new RuntimeException("No such driver",e);
		}
	}
}
