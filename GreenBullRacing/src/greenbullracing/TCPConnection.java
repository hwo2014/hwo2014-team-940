package greenbullracing;

import greenbullracing.message.ErrorMessage;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.InetSocketAddress;
import java.net.Socket;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility;
import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.introspect.VisibilityChecker;

public class TCPConnection implements Runnable, Connection {
	private static final Logger log=LoggerFactory.getLogger(TCPConnection.class);
	private static final Logger logDataflow=LoggerFactory.getLogger("dataflow");

	private static final ObjectMapper objectMapper=new ObjectMapper();

	static {
		objectMapper.setVisibilityChecker(VisibilityChecker.Std.defaultInstance().withFieldVisibility(Visibility.ANY));
		// production
//		objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES,false);
		// development
		objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES,true);
	}

	private InetSocketAddress endpoint;
	private BufferedWriter bufferedWriter;
	private BufferedReader bufferedReader;
	private DownstreamMessageHandler downHandler;
	private Socket socket;

	@SuppressWarnings("unused")
	private TCPConnection() {}

	public TCPConnection(InetSocketAddress endpoint) {
		this.endpoint=endpoint;
	}

	public void connect() throws IOException {
		log.debug("Connect to {}",endpoint);
		socket=new Socket();
		socket.setTcpNoDelay(true);
		socket.connect(endpoint);
		bufferedWriter=new BufferedWriter(new OutputStreamWriter(socket.getOutputStream(),"utf-8"));
		bufferedReader=new BufferedReader(new InputStreamReader(socket.getInputStream(),"utf-8"));
	}

	public void run() {
		try {
			Message message;
			while((message=readMessage())!=null) {
				if(message instanceof ErrorMessage)
					log.error("Server error: {}",((ErrorMessage)message).getErrorText());
				downHandler.receive(message);
			}
		} catch(IOException e) {
			log.error("Communications failure",e);
		}
		log.debug("Done.");
	}

	// UpstreamMessageHandler
	@Override
	public void send(Message message) throws JsonGenerationException, JsonMappingException, IOException {
		if(message instanceof ServerMessage) {
			// rate limit needs to be handled on a higher tier
			String s=objectMapper.writerWithType(message.getClass()).writeValueAsString(message);
			write(s);
		}
	}

	// DownstreamMessageSender
	@Override
	public void setDownstreamMessageHandler(DownstreamMessageHandler downHandler) {
		this.downHandler=downHandler;
	}

	private Message readMessage() throws IOException, JsonProcessingException {
		String s=read();
		if(s==null) {
			return null;
		}
		return objectMapper.readValue(s,Message.class);
	}

	private void write(String s) throws IOException {
		log.debug("> {}",s);
		logDataflow.debug(">\t{}\t{} ",System.currentTimeMillis(),s);
		bufferedWriter.write(s);
		bufferedWriter.write('\n');
		bufferedWriter.flush();
	}

	private String read() throws IOException {
		String s=bufferedReader.readLine();
		log.debug("< {}",s);
		logDataflow.debug("<\t{}\t{}",System.currentTimeMillis(),s);
		return s;
	}
	
}
