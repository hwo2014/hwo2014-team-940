/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package greenbullracing.route;

/**
 *
 * @author dogo
 */
public class Edge {
	private double weight;
	private final Vertex sourceVertex;
	private final Vertex destinationVertex;
	private final int pieceIndex;
	private final int startLane;
	private final int endLane;
	
	public Edge(Vertex sourceVertex, Vertex destinationVertex, double weight, int pieceIndex, int startLane, int endLane) {

    this.sourceVertex = sourceVertex;
    this.destinationVertex = destinationVertex;
    this.weight = weight;
	this.pieceIndex = pieceIndex;
	this.startLane = startLane;
	this.endLane = endLane;
  }
	@Override
	public String toString() {
		return "piece: " + pieceIndex + ", startLane: " + startLane + ", endLane: " + endLane + ", weight: " + weight + ", srcVertex: " + sourceVertex.getId() + ", dstVertex: " + destinationVertex.getId();
	}
	
	public double getWeight() {
		return weight;
	}
        
	public void setWeight(double weight) {
		this.weight = weight;
	}
	
	public int getPieceIndex() {
		return pieceIndex;
	}
	
	public int getStartLane() {
		return startLane;
	}
	
	public int getEndLane() {
		return endLane;
	}
}
