/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package greenbullracing.route;

import java.util.List;

/**
 *
 * @author dogo
 */
public class Graph {
	private final List<Vertex> vertexen;
	private final List<Edge> edges;
	private int lanecount;
	
	public Graph(List<Vertex> vertexen, List<Edge> edges, int lanecount) {
    this.vertexen = vertexen;
    this.edges = edges;
	this.lanecount = lanecount;
  }

  public List<Vertex> getVertexen() {
    return vertexen;
  }

  public List<Edge> getEdges() {
    return edges;
  }
  
  public double getLaneLength(int pieceIndex, int startLane, int endLane) {
	  
      if (startLane == endLane) {
		  return edges.get((lanecount * pieceIndex) + startLane).getWeight();
	  }
	  if (startLane < endLane) {
		  return edges.get((lanecount * pieceIndex) + startLane + (lanecount - 1)).getWeight();
	  }
	  // took left switch, hairier to count edge number.
	  return edges.get( (2 * (lanecount - ((lanecount * pieceIndex) + startLane) % lanecount) - 1) + lanecount) .getWeight();
  }
}
