/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package greenbullracing.route;

/**
 *
 * @author dogo
 */
public class Vertex {
	/* id = numlanes * pieceindex + lanenum */
	private final int id;
	
	public Vertex(int id) {
		this.id = id;
	}
	
	public int getId() {
		return this.id;
	}
}
