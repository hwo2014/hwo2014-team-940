package greenbullracing;

import greenbullracing.message.CarPositionsMessage;
import greenbullracing.message.CrashMessage;
import greenbullracing.message.CreateRaceMessage;
import greenbullracing.message.DNFMessage;
import greenbullracing.message.ErrorMessage;
import greenbullracing.message.FinishMessage;
import greenbullracing.message.GameEndMessage;
import greenbullracing.message.GameInitMessage;
import greenbullracing.message.GameStartMessage;
import greenbullracing.message.JoinMessage;
import greenbullracing.message.JoinRaceMessage;
import greenbullracing.message.LapFinishedMessage;
import greenbullracing.message.PingMessage;
import greenbullracing.message.SpawnMessage;
import greenbullracing.message.SwitchLaneMessage;
import greenbullracing.message.ThrottleMessage;
import greenbullracing.message.TournamentEndMessage;
import greenbullracing.message.TurboAvailableMessage;
import greenbullracing.message.TurboEndMessage;
import greenbullracing.message.TurboMessage;
import greenbullracing.message.TurboStartMessage;
import greenbullracing.message.YourCarMessage;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;

@JsonTypeInfo(
	use=JsonTypeInfo.Id.NAME,
	include=JsonTypeInfo.As.PROPERTY,
	property="msgType")
@JsonSubTypes({
	@JsonSubTypes.Type(value=JoinMessage.class,name="join")
	,@JsonSubTypes.Type(value=YourCarMessage.class,name="yourCar")
	,@JsonSubTypes.Type(value=GameInitMessage.class,name="gameInit")
	,@JsonSubTypes.Type(value=GameStartMessage.class,name="gameStart")
	,@JsonSubTypes.Type(value=CarPositionsMessage.class,name="carPositions")
	,@JsonSubTypes.Type(value=ThrottleMessage.class,name="throttle")
	,@JsonSubTypes.Type(value=GameEndMessage.class,name="gameEnd")
	,@JsonSubTypes.Type(value=TournamentEndMessage.class,name="tournamentEnd")
	,@JsonSubTypes.Type(value=CrashMessage.class,name="crash")
	,@JsonSubTypes.Type(value=SpawnMessage.class,name="spawn")
	,@JsonSubTypes.Type(value=LapFinishedMessage.class,name="lapFinished")
	,@JsonSubTypes.Type(value=DNFMessage.class,name="dnf")
	,@JsonSubTypes.Type(value=FinishMessage.class,name="finish")
	,@JsonSubTypes.Type(value=SwitchLaneMessage.class,name="switchLane")
	,@JsonSubTypes.Type(value=CreateRaceMessage.class,name="createRace")
	,@JsonSubTypes.Type(value=JoinRaceMessage.class,name="joinRace")
	,@JsonSubTypes.Type(value=PingMessage.class,name="ping")
	,@JsonSubTypes.Type(value=ErrorMessage.class,name="error")
	,@JsonSubTypes.Type(value=TurboAvailableMessage.class,name="turboAvailable")
	,@JsonSubTypes.Type(value=TurboMessage.class,name="turbo")
	,@JsonSubTypes.Type(value=TurboStartMessage.class,name="turboStart")
	,@JsonSubTypes.Type(value=TurboEndMessage.class,name="turboEnd")
})
public interface Message {

}
